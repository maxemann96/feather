/*
 *    Copyright [2022] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.civicrm

import dev.maximilian.feather.civicrm.internal.ParamJsonBuilder
import dev.maximilian.feather.civicrm.internal.ParamStringBuilder
import kotlinx.serialization.json.JsonPrimitive
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class BodyStringBuildTest {
    @Test
    fun `ParamJsonBuilder can build where clause of first_name=A and last_name=B correctly`() {
        val bodyContent =
            "params=" +
                ParamJsonBuilder.where(
                    setOf(
                        Triple("first_name", "=", "A"),
                        Triple("last_name", "=", "B"),
                    ),
                    "",
                )
        assertEquals("""params={"where":[["first_name","=","A"],["last_name","=","B"]]}""", bodyContent)
    }

    @Test
    fun `ParamStringBuilder can configure where clause of email correctly`() {
        val a = "params=${ParamStringBuilder.where(setOf(Triple("email", "=", "civitest999@example.org")), "")}"
        assertEquals("params=%7B%22where%22%3A%5B%5B%22email%22%2C%22%3D%22%2C%22civitest999%40example.org%22%5D%5D%7D", a)
    }

    @Test
    fun `ParamStringBuilder can configure where clause of name = Administrators correctly`() {
        // 'where' => [ ['name', '=', 'Administrators'] ]
        val bodyContent = "params=" + ParamStringBuilder.where(setOf(Triple("name", "=", "Administrators")), "")
        assertEquals("params=%7B%22where%22%3A%5B%5B%22name%22%2C%22%3D%22%2C%22Administrators%22%5D%5D%7D", bodyContent)
    }

    @Test
    fun `ParamJsonBuilder can configure value clause of display_name = Harry correctly`() {
        val bodyContent = "params=" + ParamJsonBuilder.value(mapOf("display_name" to JsonPrimitive("Harry")))
        // val bodyContentPure = "params=" + buildWhereClausePure(setOf(Triple("name", "=", "Administrators")))
        assertEquals("params={\"values\":{\"display_name\":\"Harry\"}}", bodyContent)
    }

    @Test
    fun `ParamJsonBuilder can configure value clause of display_name = Harry, first_name = BBB correctly`() {
        val bodyContent = "params=" + ParamJsonBuilder.value(mapOf("display_name" to JsonPrimitive("Harry"), "first_name" to JsonPrimitive("BBB")))
        // val bodyContentPure = "params=" + buildWhereClausePure(setOf(Triple("name", "=", "Administrators")))
        assertEquals("params={\"values\":{\"display_name\":\"Harry\",\"first_name\":\"BBB\"}}", bodyContent)
    }
}
