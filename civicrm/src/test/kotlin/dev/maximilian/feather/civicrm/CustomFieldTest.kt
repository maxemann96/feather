/*
 *    Copyright [2023] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.civicrm

import dev.maximilian.feather.civicrm.helper.CiviCRMTestSetup
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import kotlin.test.assertEquals
import kotlin.test.assertTrue
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CustomFieldTest {
    val civicrm = CiviCRMFactory.create(CiviCRMTestSetup.civiCRMSettings)

    @Test
    fun `Create custom field`() {
        runBlocking {
            val g = civicrm.createCustomGroup("testgroupCF", "testtitleCF")
            val t = civicrm.createCustomField(g.id, "testcf", "String", "abc", "testcf", "Alphanumeric")
            assertEquals("testcf", t.name)
            assertEquals("String", t.dataType)
            assertEquals("abc", t.defaultValue)
        }
    }

    @Test
    fun `Delete custom field`() {
        runBlocking {
            civicrm.getCustomField().onEach { civicrm.deleteCustomField(it.id) }
            val g = civicrm.createCustomGroup("testgroup", "testtitle")
            val t = civicrm.createCustomField(g.id, "testf", "String", "abc", "abc", "Alphanumeric")
            civicrm.deleteCustomField(t.id)
            val newList = civicrm.getCustomField()
            assertTrue(newList.isEmpty(), "List not empty after deletion")
        }
    }

    @Test
    fun `Read all custom fields return at least one result`() {
        runBlocking {
            val g = civicrm.createCustomGroup("testgroupB", "testtitleB")
            civicrm.createCustomField(g.id, "testfB", "String", "abc", "testfB", "Alphanumeric")

            val t = civicrm.getCustomField()
            assertTrue(t.isNotEmpty())
        }
    }
}
