/*
 *    Copyright [2023] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.civicrm

import dev.maximilian.feather.User
import dev.maximilian.feather.civicrm.helper.CiviCRMTestSetup
import dev.maximilian.feather.civicrm.helper.CredentialScenario
import dev.maximilian.feather.testutils.ServiceConfig
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import java.time.Instant
import kotlin.random.Random
import kotlin.test.assertEquals
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ServiceTestCheckUser {
    data class TestEnvironment(
        val service: CiviCRMService,
        val scenario: CredentialScenario,
    )

    private fun createRandomScenario(uniquePrefix: String): TestEnvironment {
        val testSetup = CiviCRMTestSetup()
        testSetup.reset()
        ServiceTest.createContactTypes(testSetup.civiService.civicrm)
        val randomNumber = Random.nextInt()
        val a = TestEnvironment(testSetup.civiService, CredentialScenario().createCRMGroupWithUser(randomNumber, uniquePrefix))
        runBlocking {
            testSetup.civiService.civicrm.createGroup(a.scenario.group.name, "CRM users of RG Test")
        }
        return a
    }

    @Test
    fun `CiviCRMService checkUser detects correctly active civicrm user as OK`() {
        val p = createRandomScenario("checkuser-ok1")
        runBlocking {
            p.service.userAddedToGroup(p.scenario.admin, p.scenario.group, p.scenario.testUser.id)
            val result = p.service.checkUser(p.scenario.testUser)

            p.scenario.deleteScenario()
            assertEquals("OK", result)
        }
    }

    @Test
    fun `CiviCRMService checkUser detects correctly not civicrm user as OK`() {
        val p = createRandomScenario("checkuser-interested")

        runBlocking {
            val t =
                User(
                    0, "notfoundtest", "NotFoundTest", "not", "foundtest",
                    "mail@example.org", emptySet(), Instant.now(), emptySet(), emptySet(),
                    false, Instant.now(),
                )
            val createdUser = ServiceConfig.CREDENTIAL_PROVIDER.createUser(t)
            val result = p.service.checkUser(createdUser)
            ServiceConfig.CREDENTIAL_PROVIDER.deleteUser(createdUser)
            p.scenario.deleteScenario()

            assertEquals("OK", result)
        }
    }

    @Test
    fun `CiviCRMService checkUser detects correctly blocked civicrm user as OK`() {
        val p = createRandomScenario("checkuser-blockedok")

        runBlocking {
            ServiceConfig.CREDENTIAL_PROVIDER.updateUser(p.scenario.testUser.copy(disabled = true))
            p.scenario.updateUser()
            p.service.userAddedToGroup(p.scenario.admin, p.scenario.group, p.scenario.testUser.id)
            val result = p.service.checkUser(p.scenario.testUser)

            p.scenario.deleteScenario()
            assertEquals("OK", result)
        }
    }

    @Test
    fun `CiviCRMService checkUser detects not blocked user in civicrm as -Not blocked in CiviCRM-`() {
        val p = createRandomScenario("checkuser-notblockedcivi")

        runBlocking {
            p.service.userAddedToGroup(p.scenario.admin, p.scenario.group, p.scenario.testUser.id)
            ServiceConfig.CREDENTIAL_PROVIDER.updateUser(p.scenario.testUser.copy(disabled = true))
            p.scenario.updateUser()
            val result = p.service.checkUser(p.scenario.testUser)

            p.scenario.deleteScenario()
            assertEquals("Not blocked in CiviCRM", result)
        }
    }

    @Test
    fun `CiviCRMService checkUser detects wrongly blocked civicrm user as -Not blocked in CiviCRM-`() {
        val p = createRandomScenario("checkuser-blockedcivi")

        runBlocking {
            p.service.userAddedToGroup(p.scenario.admin, p.scenario.group, p.scenario.testUser.id)
            ServiceConfig.CREDENTIAL_PROVIDER.updateUser(p.scenario.testUser.copy(disabled = true))
            p.scenario.updateUser()
            val result = p.service.checkUser(p.scenario.testUser)

            p.scenario.deleteScenario()
            assertEquals("Not blocked in CiviCRM", result)
        }
    }

    @Test
    fun `CiviCRMService checkUser detects wrongly blocked civicrm user as -Blocked in CiviCRM-`() {
        val p = createRandomScenario("checkuser-blockedfound")
        runBlocking {
            ServiceConfig.CREDENTIAL_PROVIDER.updateUser(p.scenario.testUser.copy(disabled = true))
            p.service.userAddedToGroup(p.scenario.admin, p.scenario.group, p.scenario.testUser.id)
            ServiceConfig.CREDENTIAL_PROVIDER.updateUser(p.scenario.testUser.copy(disabled = false))
            p.scenario.updateUser()
            val result = p.service.checkUser(p.scenario.testUser)

            p.scenario.deleteScenario()
            assertEquals("Blocked in CiviCRM", result)
        }
    }

    @Test
    fun `CiviCRMService checkUser detects not existing civicrm user as -Not found-`() {
        val p = createRandomScenario("checkuser-notfound")
        runBlocking {
            p.service.userAddedToGroup(p.scenario.admin, p.scenario.group, p.scenario.testUser.id)
            val t =
                User(
                    0, "notfoundtest", "NotFoundTest", "not", "foundtest",
                    "mail@example.org", setOf(p.scenario.group.id), Instant.now(), emptySet(), emptySet(),
                    false, Instant.now(),
                )
            val createdUser = ServiceConfig.CREDENTIAL_PROVIDER.createUser(t, "secret123")
            val result = p.service.checkUser(createdUser)
            ServiceConfig.CREDENTIAL_PROVIDER.deleteUser(createdUser)
            p.scenario.deleteScenario()

            assertEquals("Not found", result)
        }
    }

    @Test
    fun `CiviCRMService checkUser can autorepair blocked user in civicrm`() {
        val p = createRandomScenario("repairuser-notblockedcivi")

        runBlocking {
            p.service.userAddedToGroup(p.scenario.admin, p.scenario.group, p.scenario.testUser.id)
            ServiceConfig.CREDENTIAL_PROVIDER.updateUser(p.scenario.testUser.copy(disabled = true))
            p.scenario.updateUser()
            val result = p.service.checkUser(p.scenario.testUser, true)
            val result2 = p.service.checkUser(p.scenario.testUser)

            p.scenario.deleteScenario()
            assertEquals("OK", result)
            assertEquals("OK", result2)
        }
    }

    @Test
    fun `CiviCRMService checkUser can autorepair user detects wrongly blocked civicrm user as NOK (Type 2)`() {
        val p = createRandomScenario("repairuser-blockedcivi")

        runBlocking {
            ServiceConfig.CREDENTIAL_PROVIDER.updateUser(p.scenario.testUser.copy(disabled = true))
            p.service.userAddedToGroup(p.scenario.admin, p.scenario.group, p.scenario.testUser.id)
            ServiceConfig.CREDENTIAL_PROVIDER.updateUser(p.scenario.testUser.copy(disabled = false))
            p.scenario.updateUser()
            val result = p.service.checkUser(p.scenario.testUser, true)
            val result2 = p.service.checkUser(p.scenario.testUser)

            p.scenario.deleteScenario()
            assertEquals("OK", result)
            assertEquals("OK", result2)
        }
    }

    @Test
    fun `CiviCRMService checkUser can autorepair wrongly deleted civicrm user as Not found`() {
        val p = createRandomScenario("repairuser-notfound")
        runBlocking {
            ServiceConfig.CREDENTIAL_PROVIDER.updateUser(p.scenario.testUser.copy(disabled = true))
            p.service.userAddedToGroup(p.scenario.admin, p.scenario.group, p.scenario.testUser.id)
            ServiceConfig.CREDENTIAL_PROVIDER.updateUser(p.scenario.testUser.copy(disabled = false))
            p.scenario.updateUser()
            val result = p.service.checkUser(p.scenario.testUser, true)
            val result2 = p.service.checkUser(p.scenario.testUser)

            p.scenario.deleteScenario()

            assertEquals("OK", result)
            assertEquals("OK", result2)
        }
    }
}
