package dev.maximilian.feather.civicrm

import dev.maximilian.feather.civicrm.helper.CiviCRMTestSetup
import kotlinx.coroutines.runBlocking
import kotlin.test.Test
import kotlin.test.assertTrue

class CiviRulesTriggerTest {
    val civicrm = CiviCRMFactory.create(CiviCRMTestSetup.civiCRMSettings)

    @Test
    fun `Get available triggers`() {
        runBlocking {
            val response = civicrm.getCiviRulesTriggers()
            assertTrue { response.any() }
            assertTrue(response.any { it.name == "new_contact" })
            assertTrue { response.any { it.name == "new_contact" && it.id > 0 } }
        }
    }
}
