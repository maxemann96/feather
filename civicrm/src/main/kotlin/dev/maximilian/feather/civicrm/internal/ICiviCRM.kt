/*
 *    Copyright [2022] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.civicrm.internal

import dev.maximilian.feather.civicrm.CreateGroupResult
import dev.maximilian.feather.civicrm.internal.api.IACLApi
import dev.maximilian.feather.civicrm.internal.api.IACLEntityRoleApi
import dev.maximilian.feather.civicrm.internal.api.ICiviRulesActionApi
import dev.maximilian.feather.civicrm.internal.api.ICiviRulesConditionApi
import dev.maximilian.feather.civicrm.internal.api.ICiviRulesRuleActionApi
import dev.maximilian.feather.civicrm.internal.api.ICiviRulesRuleApi
import dev.maximilian.feather.civicrm.internal.api.ICiviRulesRuleConditionApi
import dev.maximilian.feather.civicrm.internal.api.ICiviRulesTriggerApi
import dev.maximilian.feather.civicrm.internal.api.IContactApi
import dev.maximilian.feather.civicrm.internal.api.IContactTypeApi
import dev.maximilian.feather.civicrm.internal.api.ICustomFieldApi
import dev.maximilian.feather.civicrm.internal.api.ICustomGroupApi
import dev.maximilian.feather.civicrm.internal.api.IEmailApi
import dev.maximilian.feather.civicrm.internal.api.IGroupApi
import dev.maximilian.feather.civicrm.internal.api.IGroupContactApi
import dev.maximilian.feather.civicrm.internal.api.IOptionGroupApi
import dev.maximilian.feather.civicrm.internal.api.IOptionValueApi

public interface ICiviCRM :
    IACLApi,
    IACLEntityRoleApi,
    IContactApi,
    IGroupApi,
    IEmailApi,
    IGroupContactApi,
    IOptionGroupApi,
    IOptionValueApi,
    IContactTypeApi,
    ICustomGroupApi,
    ICustomFieldApi,
    ICiviRulesRuleApi,
    ICiviRulesTriggerApi,
    ICiviRulesActionApi,
    ICiviRulesRuleActionApi,
    ICiviRulesConditionApi,
    ICiviRulesRuleConditionApi {
    public suspend fun getRoleIndexByName(n: String): Int?

    public suspend fun createOrReplaceTopLevelGroupWithACLPermission(
        name: String,
        title: String,
        roleName: String,
    ): CreateGroupResult

    public suspend fun createOrReplaceSubgroupWithACLPermission(
        name: String,
        title: String,
        roleName: String,
        topLevelRole: String,
    ): CreateGroupResult

    public suspend fun deleteGroupWithAclAndContactDependencies(civiGroupName: String)

    public suspend fun deleteRoleWithAclDependencies(civiRoleNameSuffix: String)
}
