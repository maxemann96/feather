/*
 * Copyright [2024] Feather development team, see AUTHORS.md
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dev.maximilian.feather.civicrm.internal.api

import dev.maximilian.feather.civicrm.entities.CiviRulesCondition
import dev.maximilian.feather.civicrm.internal.civicrm.GeneralAPI
import io.ktor.client.HttpClient
import mu.KLogging

public interface ICiviRulesConditionApi {
    public suspend fun getCiviRuleConditions(): List<CiviRulesCondition>
}
public class CiviRulesConditionApi(
    client: HttpClient,
    baseUrl: String,
    apiKey: String,
) : ICiviRulesConditionApi {
    private companion object : KLogging()
    private val generalApi = GeneralAPI(client, baseUrl, "CiviRulesCondition", null)

    public override suspend fun getCiviRuleConditions(): List<CiviRulesCondition> = generalApi.getBodyValues()
}
