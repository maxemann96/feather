/*
 *    Copyright [2022] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

CREATE TABLE public.properties (
    key character varying(255) PRIMARY KEY NOT NULL,
    value text NOT NULL
);


ALTER TABLE public.properties OWNER TO feather;

INSERT INTO public.properties (key, value) VALUES ('baseurl', 'http://localhost:$FRONTEND_PORT');
INSERT INTO public.properties (key, value) VALUES ('disable_secure_cookie', 'true');
INSERT INTO public.properties (key, value) VALUES ('redis.tls', 'false');
INSERT INTO public.properties (key, value) VALUES ('redis.host', '$REDIS_HOST');
INSERT INTO public.properties (key, value) VALUES ('redis.port', '$REDIS_PORT');
INSERT INTO public.properties (key, value) VALUES ('ldap.host', '$LDAP_HOST');
INSERT INTO public.properties (key, value) VALUES ('ldap.bind.password', '$LDAP_ADMIN_PASSWORD');
INSERT INTO public.properties (key, value) VALUES ('ldap.dn.base', 'dc=example,dc=org');
INSERT INTO public.properties (key, value) VALUES ('ldap.port', '$LDAP_PORT');
INSERT INTO public.properties (key, value) VALUES ('ldap.tls', 'false');
INSERT INTO public.properties (key, value) VALUES ('ldap.bind.dn', 'cn=admin,{{ baseDn }}');
INSERT INTO public.properties (key, value) VALUES ('additional_allowed_origins', '["http://localhost:$FRONTEND_PORT", "http://127.0.0.1:$FRONTEND_PORT"]');
INSERT INTO public.properties (key, value) VALUES ('MIGRATE_OLD_IDS', '1');
