#!/bin/sh

#
#    Copyright [2021] Feather development team, see AUTHORS.md
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
#

OPENPROJECT_MIGRATE_IOG=${OPENPROJECT_MIGRATE_IOG:-0}

for var in "$@"; do
  if [ "$var" = "--iog" ]; then
    OPENPROJECT_MIGRATE_IOG=1
  else
    echo "Warning: ignoring invalid argument '$var'!" >&2
  fi
done

OPENPROJECT_DOCKER_HOST_PORT="${OPENPROJECT_DOCKER_HOST_PORT:-8081}"
OPENPROJECT_DOCKER_HOST_DATABASE_PORT="${OPENPROJECT_DOCKER_HOST_DATABASE_PORT:-5433}"

OPENPROJECT_HOST="${OPENPROJECT_HOST:-http://127.0.0.1:$OPENPROJECT_DOCKER_HOST_PORT}"
OPENPROJECT_HEALTH_URL="$OPENPROJECT_HOST/health_check"

OPENPROJECT_POSTGRES_HOST="${OPENPROJECT_POSTGRES_HOST:-127.0.0.1}"
OPENPROJECT_POSTGRES_PORT="${OPENPROJECT_POSTGRES_PORT:-$OPENPROJECT_DOCKER_HOST_DATABASE_PORT}"
OPENPROJECT_POSTGRES_USER="${OPENPROJECT_POSTGRES_USER:-openproject}"
OPENPROJECT_POSTGRES_DATABASE="${OPENPROJECT_POSTGRES_DATABASE:-openproject}"
OPENPROJECT_POSTGRES_PASSWORD="${OPENPROJECT_POSTGRES_PASSWORD:-openproject}"
