#!/bin/sh

#
#    Copyright [2021] Feather development team, see AUTHORS.md
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
#

set -e

SCRIPT_DIR="$(dirname "$0")"

cd "$SCRIPT_DIR"

. ./variables.sh
. ../local/util.sh

docker compose pull openproject openldap
docker compose rm -fsv openproject openldap || true

LDAP_ADMIN_PASSWORD=$(openssl rand -hex 32)
export LDAP_ADMIN_PASSWORD

docker compose up -d openldap
#service_ready OpenLdap 3 15 "[ \$(docker ps -a -f \"name=feather-local-openldap\" --format \"{{ .State }}\") = \"running\" ]"
service_ready OpenLdap 3 15 "docker logs feather-local-openldap 2>&1 | grep 'slapd starting' > /dev/null"

# NOTE: the iog.ldif is already part of the image
# registry.gitlab.com/ingenieure-ohne-grenzen/container_images/openldap-iog:OpenLDAP_2.6 !
#if [ "$OPENPROJECT_MIGRATE_IOG" -eq 1 ]; then
#  docker exec --user root feather-local-openldap /bin/sh -c "mkdir -p /seed"
#  docker cp "../local/seed/openldap/iog.ldif" feather-local-openldap:/seed/iog.ldif > /dev/null
#  docker exec feather-local-openldap ldapadd -H 'ldapi:///' -w "$LDAP_ADMIN_PASSWORD" -D "cn=admin,dc=example,dc=org" -f /seed/iog.ldif > /dev/null
#fi;

docker compose up -d openproject
service_ready OpenProject 10 150 "curl --fail -s -o /dev/null \"$OPENPROJECT_HEALTH_URL\" > /dev/null 2>&1"

OPENPROJECT_MIGRATE_IOG=$OPENPROJECT_MIGRATE_IOG "./migrate_db.sh"

# Sync registered users
echo "Ldap::SynchronizationJob::perform_now()" | docker exec -i openproject bundle exec rails console # > /dev/null

echo "LDAP_ADMIN_PASSWORD=$LDAP_ADMIN_PASSWORD"
