#!/bin/bash

#
#    Copyright [2021] Maximilian Hippler <hello@maximilian.dev>
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
#

set -e

# SCRIPT_DIR="$(dirname "$0")"
#. "$SCRIPT_DIR/variables.sh"

# only tests that the port is open
civicrm_ready_nc() {
  nc -z -v $1 $2
  return $?
}

# tests that we get the expected web response
civicrm_ready_curl() {
  status_code=$(curl -s -o /dev/null "http://$1:$2" -w "%{http_code}")
  #echo "status_code: $status_code"
  if [ "$1" = "localhost" ]; then
    if [ $status_code -eq 400 ]; then
      # as DRUPAL_TRUSTED_HOST != localhost, we get "400 Bad Request"
      return 0
    fi
  fi
  if [ $status_code -eq 200 ]; then
    return 0
  elif [ $status_code -eq 403 ]; then
    # without login, any request returns "403 forbidden" (which is wrong, it should be "402 Unauthorized")
    return 0
  else
    return 1
  fi
}

civicrm_ready() {
  if [ "$1" = "localhost" ]; then
    # for local testing, check with curl
    civicrm_ready_curl $1 $2
  else
    # curl always gives status code 000 in CI???
    # Thus, use simple port test.
    civicrm_ready_nc $1 $2
  fi
  return $?
}

if [[ "$CIVICRM_HOST" =~ ^(https?)://([^/]+):([1-9][0-9]*)$ ]]; then
  CIVICRM_HOST="${BASH_REMATCH[2]}"
  CIVICRM_PORT="${BASH_REMATCH[3]}"
elif [[ "$CIVICRM_HOST" =~ ^(https?)://([^/]+)$ ]]; then
  CIVICRM_HOST="${BASH_REMATCH[2]}"
  CIVICRM_PORT=8083
else
  CIVICRM_HOST=${CIVICRM_HOST:-civicrm}
  CIVICRM_PORT=8083
fi

set +e
WAITED=0
while ! civicrm_ready $CIVICRM_HOST $CIVICRM_PORT; do
    sleep 5
    WAITED=$((WAITED+5))
    echo "Waited $WAITED seconds on CiviCRM to be ready"
done

set -e
echo "CiviCRM ready"
