/*
 *
 *  *    Copyright [2020] Feather development team, see AUTHORS.md
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *        http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package dev.maximilian.feather.nextcloud.ocs

import dev.maximilian.feather.nextcloud.Ocs
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.forms.submitForm
import io.ktor.client.request.get
import io.ktor.http.parameters
import kotlinx.serialization.Serializable

internal class GroupAPI(private val client: HttpClient, baseUrl: String) : IGroupAPI {
    private val basePath = "ocs/v1.php/cloud"
    private val groupsPath = "$baseUrl/$basePath/groups"

    override suspend fun addGroup(groupId: String): Boolean =
        client.submitForm(
            url = groupsPath,
            formParameters =
            parameters {
                append("groupid", groupId)
            },
        ).body<Ocs<List<String>>>().ocs.meta.statusCode.let {
            it == 100 || it == 102
        }

    override suspend fun getAllGroups(): List<String> = client.get(groupsPath).body<Ocs<GroupList>>().ocs.data.groups
}

@Serializable
private data class GroupList(
    val groups: List<String>,
)
