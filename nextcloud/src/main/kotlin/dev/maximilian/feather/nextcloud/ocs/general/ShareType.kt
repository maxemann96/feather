/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.nextcloud.ocs.general

import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

@Serializable(with = ShareTypeSerializer::class)
public enum class ShareType(internal val q: Int) {
    User(0),
    Group(1),
    PublicLink(3),
    FederatedCloudShare(6),
}

internal object ShareTypeSerializer : KSerializer<ShareType> {
    override val descriptor: SerialDescriptor = PrimitiveSerialDescriptor("shareType", PrimitiveKind.INT)

    override fun serialize(
        encoder: Encoder,
        value: ShareType,
    ) {
        encoder.encodeInt(value.q)
    }

    override fun deserialize(decoder: Decoder): ShareType =
        decoder.decodeInt().let { shareTypeId ->
            requireNotNull(ShareType.values().firstOrNull { it.q == shareTypeId }) {
                "ShareType with id $shareTypeId not found"
            }
        }
}
