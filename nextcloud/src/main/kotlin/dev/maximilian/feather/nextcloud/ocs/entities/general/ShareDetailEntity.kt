/*
 *
 *  *    Copyright [2020] Feather development team, see AUTHORS.md
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *        http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package dev.maximilian.feather.nextcloud.ocs.entities.general

import dev.maximilian.feather.nextcloud.ocs.general.PermissionType
import dev.maximilian.feather.nextcloud.ocs.general.PermissionTypeSetSerializer
import dev.maximilian.feather.nextcloud.ocs.general.ShareType
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
public data class ShareDetailEntity(
    @SerialName("id")
    val id: Int,
    @SerialName("share_type")
    val shareType: ShareType,
    @SerialName("uid_owner")
    val uidOwner: String,
    @SerialName("displayname_file_owner")
    val displayNameFileOwner: String,
    @SerialName("displayname_owner")
    val displayNameOwner: String,
    @SerialName("permissions")
    @Serializable(with = PermissionTypeSetSerializer::class)
    val permissions: Set<PermissionType>,
    @SerialName("can_edit")
    val canEdit: Boolean,
    @SerialName("can_delete")
    val canDelete: Boolean,
    @SerialName("token")
    val token: String? = null,
    @SerialName("url")
    val url: String? = null,
    @SerialName("expiration")
    val expiration: String? = null,
    @SerialName("path")
    val path: String,
    @SerialName("item_type")
    val itemType: String,
)
