/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.nextcloud.ocs

import dev.maximilian.feather.nextcloud.Ocs
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.forms.submitForm
import io.ktor.client.request.get
import io.ktor.http.parameters
import kotlinx.serialization.Serializable

internal class ShareAPISettingsAPI(
    private val client: HttpClient,
    baseUrl: String,
) : IShareAPISettingsAPI {
    private val basePath = "ocs/v2.php/apps/provisioning_api/api/v1"
    private val coreSettingsPath = "$baseUrl/$basePath/config/apps/core"

    // default expire date

    override suspend fun getDefaultExpireDate(): Boolean =
        client.get("$coreSettingsPath/shareapi_default_expire_date")
            .body<Ocs<StringResponseData>>().ocs.data.data == "yes"

    override suspend fun setDefaultExpireDate(defaultExpireDate: Boolean): Boolean =
        client.submitForm(
            url = "$coreSettingsPath/shareapi_default_expire_date",
            formParameters =
            parameters {
                append("value", if (defaultExpireDate) "yes" else "no")
            },
        ).body<Ocs<List<String>>>().ocs.meta.statusCode == 200

    // default internal expire date

    override suspend fun getDefaultInternalExpireDate(): Boolean =
        client.get("$coreSettingsPath/shareapi_default_internal_expire_date")
            .body<Ocs<StringResponseData>>().ocs.data.data == "yes"

    override suspend fun setDefaultInternalExpireDate(defaultExpireDate: Boolean): Boolean =
        client.submitForm(
            url = "$coreSettingsPath/shareapi_default_internal_expire_date",
            formParameters =
            parameters {
                append("value", if (defaultExpireDate) "yes" else "no")
            },
        ).body<Ocs<List<String>>>().ocs.meta.statusCode == 200

    // expire after n days
    override suspend fun getExpireAfterNDays(): Number =
        client.get("$coreSettingsPath/shareapi_expire_after_n_days").body<Ocs<IntResponseData>>().ocs.data.data

    override suspend fun setExpireAfterNDays(expireAfterNDays: Number): Boolean =
        client.submitForm(
            url = "$coreSettingsPath/shareapi_expire_after_n_days",
            formParameters =
            parameters {
                append("value", "$expireAfterNDays")
            },
        ).body<Ocs<List<String>>>().ocs.meta.statusCode == 200

    // internal expire after n days
    override suspend fun getInternalExpireAfterNDays(): Number =
        client.get("$coreSettingsPath/shareapi_internal_expire_after_n_days")
            .body<Ocs<IntResponseData>>().ocs.data.data

    override suspend fun setInternalExpireAfterNDays(expireAfterNDays: Number): Boolean =
        client.submitForm(
            url = "$coreSettingsPath/shareapi_internal_expire_after_n_days",
            formParameters =
            parameters {
                append("value", "$expireAfterNDays")
            },
        ).body<Ocs<List<String>>>().ocs.meta.statusCode == 200

    // enforce expire date

    override suspend fun getEnforceExpireDate(): Boolean =
        client.get("$coreSettingsPath/shareapi_enforce_expire_date")
            .body<Ocs<StringResponseData>>().ocs.data.data == "yes"

    override suspend fun setEnforceExpireDate(enforceExpireDate: Boolean): Boolean =
        client.submitForm(
            url = "$coreSettingsPath/shareapi_enforce_expire_date",
            formParameters =
            parameters {
                append("value", if (enforceExpireDate) "yes" else "no")
            },
        ).body<Ocs<List<String>>>().ocs.meta.statusCode == 200

    // enforce internal expire date

    override suspend fun getEnforceInternalExpireDate(): Boolean =
        client.get("$coreSettingsPath/shareapi_enforce_internal_expire_date")
            .body<Ocs<StringResponseData>>().ocs.data.data == "yes"

    override suspend fun setEnforceInternalExpireDate(enforceExpireDate: Boolean): Boolean =
        client.submitForm(
            url = "$coreSettingsPath/shareapi_enforce_internal_expire_date",
            formParameters =
            parameters {
                append("value", if (enforceExpireDate) "yes" else "no")
            },
        ).body<Ocs<List<String>>>().ocs.meta.statusCode == 200
}

@Serializable
private data class StringResponseData(
    val data: String,
)

@Serializable
private data class IntResponseData(
    val data: Int,
)
