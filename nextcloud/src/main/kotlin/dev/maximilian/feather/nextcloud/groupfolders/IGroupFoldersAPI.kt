/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.nextcloud.groupfolders

import dev.maximilian.feather.nextcloud.groupfolders.entities.GroupFolderDetails

public interface IGroupFoldersAPI {
    public suspend fun getAllGroupFolders(): Map<String, GroupFolderDetails>

    public suspend fun getGroupFolder(id: Int): GroupFolderDetails

    public suspend fun createGroupFolder(mountpoint: String): Int

    // newQuota is as string encoding the number of bytes or "-3" for no quota
    public suspend fun changeQuota(
        id: Int,
        newQuota: String,
    ): Boolean

    public suspend fun changeMointpoint(
        id: Int,
        newMountpoint: String,
    ): Boolean

    public suspend fun changeAclFlag(
        id: Int,
        aclFlag: Boolean,
    ): Boolean

    // mappingType is either 'user' or 'group' and mappingId the id of the user or group
    public suspend fun changeAclManager(
        id: Int,
        mappingType: String,
        mappingId: String,
        manageAcl: Boolean,
    ): Boolean

    public suspend fun deleteGroupFolder(id: Int)
}
