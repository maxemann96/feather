/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather

public interface ICredentialProvider {
    public fun getUser(id: Int): User?

    public fun getUserByExternalId(id: String): User?

    public fun getUserExternalId(user: User): String?

    public fun getUserByMail(mail: String): User?

    public fun getUserByUsername(username: String): User?

    public fun getPhotoForUser(user: User): ByteArray?

    public fun updatePhotoForUser(
        user: User,
        image: ByteArray,
    )

    public fun deletePhotoForUser(user: User)

    // Returns all available users
    public fun getUsers(): Collection<User>

    // Creates a user and returns a copy of this user with the id field being correctly filled
    public fun createUser(
        user: User,
        password: String? = null,
    ): User

    // Deletes a User
    public fun deleteUser(user: User)

    // Update a User, the userbackend should retrieve the actual value from the user and update it to the parameter user
    public fun updateUser(user: User): User?

    public fun updateLastLoginOf(user: User): User?

    public fun authenticateUserByUsernameOrMail(
        usernameOrMail: String,
        password: String,
    ): User?

    public fun updateUserPassword(
        user: User,
        password: String,
    ): Boolean

    // Returns a specific Group or null
    public fun getGroup(id: Int): Group?

    public fun getGroupByName(groupname: String): Group?

    // Returns all available groups
    public fun getGroups(): Collection<Group>

    // Creates a group and returns a copy of this group with the id field being correctly filled
    public fun createGroup(group: Group): Group

    // Deletes a Group
    public fun deleteGroup(group: Group)

    // Update a Group, the group backend should retrieve the actual value from the user and update it to the parameter group
    public fun updateGroup(group: Group): Group?

    public fun getUsersAndGroups(): Pair<Collection<User>, Collection<Group>>

    public fun close()
}
