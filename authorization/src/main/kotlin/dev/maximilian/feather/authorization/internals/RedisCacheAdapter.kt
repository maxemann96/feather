/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.authorization.internals

import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import dev.maximilian.feather.Group
import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.User
import mu.KLogging
import redis.clients.jedis.Jedis
import redis.clients.jedis.JedisPool
import redis.clients.jedis.params.SetParams
import java.io.Closeable
import java.util.Locale
import kotlin.concurrent.timer
import kotlin.system.measureTimeMillis

internal class RedisCacheAdapter(private val pool: JedisPool, private val credentialProvider: ICredentialProvider) : Closeable by pool {
    companion object : KLogging()

    private val setParams = SetParams().ex(5 * 60) // default cache expiration time: 5 minutes
    private val objectMapper = jacksonObjectMapper().registerModule(JavaTimeModule())

    private val userIDArrayKey = "feather.users"
    private val groupIDArrayKey = "feather.groups"

    private fun getUserKey(userID: Int) = "feather.user.$userID"

    private fun getUserByMailKey(mail: String) = "feather.user.byMail.$mail"

    private fun getGroupKey(groupID: Int) = "feather.group.$groupID"

    init {
        // refresh every 4 minutes
        timer(daemon = true, period = 4 * 60 * 1000L, initialDelay = 4 * 60 * 1000L) { refreshCache() }
        refreshCache()
    }

    fun refreshCache() {
        var q: Pair<Collection<User>, Collection<Group>>
        val timeInMillisGetUser =
            measureTimeMillis {
                q = credentialProvider.getUsersAndGroups()
            }
        val users = q.first
        val groups = q.second

        val timeInMillisResync =
            measureTimeMillis {
                redisTransaction { jedis ->
                    users.forEach {
                        jedis[getUserKey(it.id)] = it
                        jedis[getUserByMailKey(it.mail.lowercase(Locale.getDefault()))] = it
                    }

                    groups.forEach { jedis[getGroupKey(it.id)] = it }

                    jedis.del(userIDArrayKey)
                    jedis.del(groupIDArrayKey)

                    if (users.isNotEmpty()) {
                        jedis.sadd(userIDArrayKey, *users.map { it.id.toString() }.toTypedArray())
                    }

                    if (groups.isNotEmpty()) {
                        jedis.sadd(groupIDArrayKey, *groups.map { it.id.toString() }.toTypedArray())
                    }
                }
            }
        if (timeInMillisGetUser < 1000 && timeInMillisResync < 1000) {
            logger.debug {
                "RedisCacheAdapter::refreshCache needed $timeInMillisGetUser ms to read ${users.size} users and ${groups.size} groups from ldap. Then $timeInMillisResync ms to write them into REDIS."
            }
        } else {
            logger.warn {
                "RedisCacheAdapter::refreshCache too slow! It needed $timeInMillisGetUser ms to read ${users.size} users and ${groups.size} groups from ldap. Then $timeInMillisResync ms to write them into REDIS."
            }
        }
    }

    fun getUsers(): Set<User> =
        redisTransaction {
            it.smembers(userIDArrayKey).mapNotNull { user -> it.getObject<User>(getUserKey(user.toInt())) }
        }.toSet()

    fun getGroups(): Set<Group> =
        redisTransaction {
            it.smembers(groupIDArrayKey).mapNotNull { group -> it.getObject<Group>(getGroupKey(group.toInt())) }
        }.toSet()

    fun getUser(id: Int): User? = redisTransaction { it.getObject<User>(getUserKey(id)) }

    fun getUserByMail(mail: String): User? =
        redisTransaction {
            it.getObject<User>(
                getUserByMailKey(
                    mail.lowercase(
                        Locale.getDefault(),
                    ),
                ),
            )
        }

    fun getUserByUsername(username: String): User? = redisTransaction { getUsers().firstOrNull { user -> user.username == username } }

    fun getGroup(id: Int): Group? = redisTransaction { it.getObject<Group>(getGroupKey(id)) }

    fun getGroupByName(groupname: String): Group? = redisTransaction { getGroups().firstOrNull { group -> group.name == groupname } }

    private fun <T> redisTransaction(block: (Jedis) -> T): T = pool.resource.use(block)

    private operator fun <T> Jedis.set(
        key: String,
        value: T,
    ): String =
        this.set(
            key,
            objectMapper.writeValueAsString(value),
            setParams,
        )

    private fun <T> Jedis.get(
        key: String,
        clazz: Class<T>,
    ): T? = this[key]?.let { objectMapper.readValue(it, clazz) }

    private inline fun <reified T> Jedis.getObject(key: String): T? = get(key, T::class.java)
}
