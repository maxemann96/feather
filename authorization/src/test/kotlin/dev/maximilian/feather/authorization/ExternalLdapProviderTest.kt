/*
 *    Copyright [2022] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.authorization

import dev.maximilian.feather.testutils.getEnv

class ExternalLdapProviderTest : ExternalProviderTestBase(externalCredentialProvider) {
    companion object {
        private val externalCredentialProvider =
            LdapProviderFactory(
                mutableMapOf(
                    "ldap.host" to getEnv("LDAP_HOST", "127.0.0.1"),
                    "ldap.port" to getEnv("LDAP_PORT", "389"),
                    "ldap.tls" to "false",
                    "ldap.bind.dn" to getEnv("LDAP_BIND_USER", "cn=admin,{{ baseDn }}"),
                    "ldap.bind.password" to getEnv("LDAP_BIND_PASSWORD", "admin"),
                    "ldap.unique_groups" to getEnv("LDAP_UNIQUE_GROUPS", "true"),
                    "ldap.plain_text_passwords" to getEnv("PLAIN_TEXT_PASSWORDS", "false"),
                    "ldap.dn.base" to getEnv("LDAP_BASE_DN", "dc=example,dc=org"),
                ),
            ).create()
    }
}
