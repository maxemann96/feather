/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.authorization

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import dev.maximilian.feather.AppBuilder
import dev.maximilian.feather.Permission
import dev.maximilian.feather.account.AccountController
import dev.maximilian.feather.action.ActionController
import dev.maximilian.feather.authorization.api.AuthorizerApi
import dev.maximilian.feather.authorization.api.LoginRequest
import dev.maximilian.feather.authorization.api.SessionAnswer
import dev.maximilian.feather.testutils.InMemoryExternalCredentialProvider
import dev.maximilian.feather.testutils.TestUser
import kong.unirest.core.Config
import kong.unirest.core.GenericType
import kong.unirest.core.HttpRequest
import kong.unirest.core.HttpResponse
import kong.unirest.core.RequestBodyEntity
import kong.unirest.core.UnirestInstance
import kong.unirest.modules.jackson.JacksonObjectMapper
import org.eclipse.jetty.http.HttpStatus
import org.jetbrains.exposed.sql.Database
import java.sql.DriverManager
import java.time.Instant
import java.util.UUID
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertNotNull

class SessionApiTest {
    companion object {
        private val db =
            Database.connect(getNewConnection = { DriverManager.getConnection("jdbc:h2:mem:feather;DB_CLOSE_DELAY=-1") })

        var authApp: AuthorizerApi? = null
        var authorizationController: AuthorizationController? = null
    }

    @Test
    fun `Create session with valid mail and password returns CREATED (201)`() {
        val port = 9001
        val (restConnection, mail, password) = loadAuthorizerAndCreateTestConnection(port)
        val session = restConnection.loginRequest(mail, password, port).asEmpty()
        assertEquals(HttpStatus.CREATED_201, session.status)
    }

    @Test
    fun `Create session method sets miniSession to false in session database`() {
        val port = 9010
        val (restConnection, mail, password) = loadAuthorizerAndCreateTestConnection(port)
        val sessionResponse = restConnection.loginRequest(mail, password, port).asObject<SessionAnswer>()

        assertNotNull(sessionResponse.body)

        val dbSession = authorizationController!!.getSession(sessionResponse.body.id)

        assertNotNull(dbSession)
        assertFalse(dbSession.miniSession)
    }

    @Test
    fun `Create session with valid username and password returns CREATED (201)`() {
        val port = 9002
        val (restConnection, mail, password) = loadAuthorizerAndCreateTestConnection(port)
        val session = restConnection.loginRequest(mail, password, port).asEmpty()
        assertEquals(HttpStatus.CREATED_201, session.status)
    }

    @Test
    fun `Create session with invalid user returns UNAUTHORIZED (401)`() {
        val port = 9003
        val (restConnection, mail, password) = loadAuthorizerAndCreateTestConnection(port)
        val session = restConnection.loginRequest("abc$mail", password, port).asEmpty()
        assertEquals(HttpStatus.UNAUTHORIZED_401, session.status)
    }

    @Test
    fun `Create session with valid user but wrong password returns UNAUTHORIZED (401)`() {
        val port = 9004
        val (restConnection, mail, _) = loadAuthorizerAndCreateTestConnection(port)
        val session = restConnection.loginRequest(mail, UUID.randomUUID().toString(), port).asEmpty()
        assertEquals(HttpStatus.UNAUTHORIZED_401, session.status)
    }

    @Test
    fun `Get session with returns OK (200) for valid session`() {
        val port = 9005
        val (restConnection, mail, password) = loadAuthorizerAndCreateTestConnection(port)
        restConnection.loginRequest(mail, password, port).asEmpty()
        val session = restConnection.get("http://127.0.0.1:$port/v1/session").asEmpty()
        assertEquals(HttpStatus.OK_200, session.status)
    }

    @Test
    fun `Get session with returns Not Found (404) for not existing session`() {
        val port = 9006
        val (restConnection, _, _) = loadAuthorizerAndCreateTestConnection(port)
        val session = restConnection.get("http://127.0.0.1:$port/v1/session").asEmpty()
        assertEquals(HttpStatus.NOT_FOUND_404, session.status)
    }

    @Test
    fun `Create session delivers correct body`() {
        val port = 9007
        val (restConnection, mail, password) = loadAuthorizerAndCreateTestConnection(port)
        val session = restConnection.loginRequest(mail, password, port).asObject<SessionAnswer>().body
        assert(session.id.toString().isNotBlank())
        assert(session.validUntil > Instant.now())
    }

    @Test
    fun `Get session delivers correct body`() {
        val port = 9008
        val (restConnection, mail, password) = loadAuthorizerAndCreateTestConnection(port)
        val firstSession = restConnection.loginRequest(mail, password, port).asObject<SessionAnswer>().body
        val sessionResponse = restConnection.get("http://127.0.0.1:$port/v1/session").asObject<SessionAnswer>()
        val session = sessionResponse.body
        assertEquals(firstSession.id, session.id)
    }

    @Test
    fun `Delete session delivers 200 (OK)`() {
        val port = 9009
        val (restConnection, mail, password) = loadAuthorizerAndCreateTestConnection(port)
        restConnection.loginRequest(mail, password, port).asObject<SessionAnswer>().body
        val session = restConnection.delete("http://127.0.0.1:$port/v1/session").asObject<String>()
        assertEquals(HttpStatus.OK_200, session.status)
        assertEquals("http://127.0.0.1/login", session.body)
    }

    private fun UnirestInstance.loginRequest(
        username: String,
        password: String,
        port: Int,
    ): RequestBodyEntity = post("http://127.0.0.1:$port/v1/authorizer/credential").body(LoginRequest(username, password))

    private fun loadAuthorizerAndCreateTestConnection(port: Int): Triple<UnirestInstance, String, String> {
        val app = AppBuilder().createApp("test", "session-api-test", "http://127.0.0.1", emptySet(), port)
        val testUser = TestUser.generateTestUser(permissions = setOf(Permission.USER))
        val password = UUID.randomUUID().toString()
        val accountController = AccountController(db, InMemoryExternalCredentialProvider(), mutableMapOf("MIGRATE_OLD_IDS" to "42"))
        accountController.createUser(testUser, password)
        authorizationController = AuthorizationController(mutableSetOf(CredentialAuthorizer("credential", accountController, "http://127.0.0.1")), accountController, db, ActionController(db, accountController))
        authApp = AuthorizerApi(app, authorizationController!!, true)
        val restUser =
            UnirestInstance(
                Config().setObjectMapper(
                    JacksonObjectMapper(
                        jacksonObjectMapper()
                            .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                            .configure(DeserializationFeature.ACCEPT_EMPTY_ARRAY_AS_NULL_OBJECT, true)
                            .registerModule(JavaTimeModule()),
                    ),
                )
                    .addDefaultHeader("Accept", "application/json"),
            )
        return Triple(restUser, testUser.mail, password)
    }

    private inline fun <reified B> HttpRequest<*>.asObject(): HttpResponse<B> = this.asObject(object : GenericType<B>() {})
}
