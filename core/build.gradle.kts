import java.time.Instant

/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

plugins {
    application
}

dependencies {
    implementation(project(":lib"))
    implementation(project(":nextcloud"))
    implementation(project(":openproject"))
    implementation(project(":kubernetes"))
    implementation(project(":authorization"))
    implementation(project(":multiservice"))
    implementation(project(":iog"))
    implementation(project(":keycloak-actions"))
    implementation(project(":civicrm"))

    // Database ORM Framework
    implementation(libs.bundles.exposed)

    // Database driver implementation
    implementation(libs.postgres)

    // Rest framework
    implementation(libs.javalin.core)
    implementation(libs.javalin.openapi.core)
    kapt(libs.javalin.openapi.kapt)

    // OpenApi Document Generator
    implementation(libs.swagger.core)
    implementation(libs.swagger.ui)

    // Redis driver
    implementation(libs.jedis)

    // Json Request Mapper
    implementation(libs.bundles.jackson)

    // Mail
    implementation(libs.mail)

    // Micrometer metrics plugin
    implementation(libs.bundles.micrometer)

    // Im memory database
    testImplementation(libs.h2)
}

application {
    applicationName = "feather"
    mainClass.set("dev.maximilian.feather.MainKt")
    applicationDefaultJvmArgs = setOf("-Djava.awt.headless=true")
}

distributions {
    main {
        distributionBaseName.set("feather")
    }
}

tasks.processResources {
    filter(
        org.apache.tools.ant.filters.ReplaceTokens::class,
        "tokens" to mapOf("version" to project.version),
    )
}

kotlin {
    explicitApi()
}

tasks {
    jar {
        manifest {
            attributes(
                "Built-By" to System.getProperty("user.name"),
                "Build-Timestamp" to Instant.now(),
                "Created-By" to "Gradle ${gradle.gradleVersion}",
                "Build-Jdk" to "${System.getProperty("java.version")} (${System.getProperty("java.vendor")} ${
                    System.getProperty(
                        "java.vm.version",
                    )
                })",
                "Build-OS" to "${System.getProperty("os.name")} ${System.getProperty("os.arch")} ${System.getProperty("os.version")}",
                "Version" to "${gradle.rootProject.version}",
            )
        }
    }
}
