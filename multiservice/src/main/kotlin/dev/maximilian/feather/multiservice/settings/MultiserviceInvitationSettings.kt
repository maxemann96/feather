package dev.maximilian.feather.multiservice.settings

data class InactiveUsersSettings(
    val reminderAfterInactivityInMonths: Int = 6,
    val deactivationAfterReminderInMonths: Int = 2,
    val deletionAfterDeactivationInMonths: Int = 1,
    val protectedGroup: String?,
)

data class MultiserviceUserManagementSettings(
    val invitationRequiresGroup: Boolean,
    val inactiveUsersSettings: InactiveUsersSettings? = null,
)
