/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.multiservice.internals.useroperations

import dev.maximilian.feather.IControllableService
import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.User
import dev.maximilian.feather.multiservice.events.UserDeletionEvent
import mu.KLogging

internal class DeleteUser(
    private val credentialProvider: ICredentialProvider,
    private val services: List<IControllableService>,
    private val userDeletionEvent: List<UserDeletionEvent>,
) {
    companion object : KLogging()

    fun deleteUser(userId: Int): User {
        logger.info { "DeleteUser::deleteUser Deleting user ($userId)" }

        val user =
            credentialProvider.getUser(userId)
                ?: throw IllegalArgumentException("DeleteUser::deleteUser User ($userId) not found")

        userDeletionEvent.forEach { it.userDeleted(user) }

        var ok = true
        services.forEach {
            if (!it.deleteUser(user)) {
                ok = false
            }
        }

        if (!ok) {
            throw IllegalStateException("Cannot delete LDAP user because of previous error")
        }

        credentialProvider.deleteUser(user)

        return user
    }
}
