/*
 *    Copyright [2020-2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.multiservice.internals.useroperations

import dev.maximilian.feather.IControllableService
import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.IServiceSupportsAvatar
import dev.maximilian.feather.User
import dev.maximilian.feather.openproject.ImageUtil
import mu.KLogging
import java.awt.image.BufferedImage
import java.io.ByteArrayOutputStream
import javax.imageio.ImageIO

internal class ChangeUserAvatar(
    private val credentialProvider: ICredentialProvider,
    private val services: List<IControllableService>,
) {
    companion object : KLogging()

    fun changeUserAvatar(
        user: User,
        newAvatar: BufferedImage,
    ) {
        logger.info { "ChangeUserAvatar::changeUserAvatar Changing avatar for user ${user.id}" }

        services.filterIsInstance<IServiceSupportsAvatar>().all {
            it.changeUserAvatar(user, newAvatar)
        } ||
            throw IllegalStateException("Cannot change user ${user.id} avatar because of previous error")

        val renderedJpegImage = ByteArrayOutputStream().apply {
            ImageIO.write(
                ImageUtil.getToMaximumScaledImage(newAvatar),
                "JPEG",
                this,
            ) ||
                throw Exception("Could not write array")
        }.toByteArray()

        credentialProvider.updatePhotoForUser(user, renderedJpegImage)
    }

    fun deleteUserAvatar(user: User) {
        logger.info { "ChangeUserAvatar::deleteUserAvatar Deleting avatar for user ${user.id}" }

        services.filterIsInstance<IServiceSupportsAvatar>().all {
            it.changeUserAvatar(user, null)
        } ||
            throw IllegalStateException("Cannot delete user avatar because of previous error")

        credentialProvider.deletePhotoForUser(user)
    }
}
