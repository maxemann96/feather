/*
 *    Copyright [2020-2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.multiservice

import dev.maximilian.feather.multiservice.mockserver.GroupSyncMock
import dev.maximilian.feather.multiservice.openproject.OpenProjectService
import dev.maximilian.feather.multiservice.settings.OpenProjectTestVariables
import dev.maximilian.feather.testutils.FakeData
import dev.maximilian.feather.testutils.ServiceConfig
import kotlinx.coroutines.runBlocking
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class OpenProjectServiceTest {
    private val backgroundJobManager = BackgroundJobManager(ApiTestUtilities.pool)
    private val groupSynchronizationEvent = GroupSyncMock()
    private val credentialProvider = ServiceConfig.ACCOUNT_CONTROLLER

    private val opService =
        OpenProjectService(backgroundJobManager, groupSynchronizationEvent, OpenProjectTestVariables.settings, credentialProvider, 3)
    private val generalServiceTest = ServiceTestUtilities(opService)

    @Test
    fun `OpenProject service name is OpenProject`() {
        val serviceName = opService.serviceName
        assertEquals("OpenProject", serviceName)
    }

    @Test
    fun `OpenProject service can create test user`() {
        val success = generalServiceTest.createSimpleUserTest()
        assertTrue(success)
    }

    @Test
    fun `delete of freshly created test user returns TRUE `() {
        val testUser = generalServiceTest.createRandomTestUser(true)

        val r = opService.deleteUser(testUser)
        assertTrue(r)
    }

    @Test
    fun `delete not existing user returns TRUE`() {
        val testUser = generalServiceTest.getRandomTestUser(true)
        val result = opService.deleteUser(testUser)
        assertTrue(result)
    }

    @Test
    fun `activateUser(FALSE) of an active test user leads to result TRUE (success)`() {
        val testUser = generalServiceTest.createRandomTestUser(true)
        val success = opService.activateUser(testUser, false)
        assertTrue(success)
    }

    @Test
    fun `activateUser(TRUE) of an inactive test user leads to result TRUE (success)`() {
        val testUser = generalServiceTest.createRandomTestUser(false)
        val success = opService.activateUser(testUser, true)
        assertTrue(success)
    }

    @Test
    fun `activateUser(FALSE) of an inactive test user leads to result TRUE (already set)`() {
        val testUser = generalServiceTest.createRandomTestUser(false)
        val success = opService.activateUser(testUser, false)
        assertTrue(success)
    }

    @Test
    fun `activateUser(TRUE) of an active test user leads to result TRUE (already set)`() {
        val testUser = generalServiceTest.createRandomTestUser(true)
        val success = opService.activateUser(testUser, true)
        assertTrue(success)
    }

    @Test
    fun `activateUser(TRUE) of an not existing user leads to FALSE (failed)`() {
        val testUser = generalServiceTest.getRandomTestUser(true)
        val success = opService.activateUser(testUser, true)
        assertFalse(success)
    }

    @Test
    fun `checkUser of an existing user leads to TRUE`() {
        val testUser = generalServiceTest.createRandomTestUser(true)
        val resultString = runBlocking { opService.checkUser(testUser) }
        assertEquals("OK", resultString)
    }

    @Test
    fun `checkUser of an existing user with wrong email leads to Mail address differs message`() {
        val testUser = generalServiceTest.createRandomTestUser(true)
        val testUserWrong = testUser.copy(mail = FakeData.generateMailAddress())
        val resultString = runBlocking { opService.checkUser(testUserWrong) }
        assertEquals("Mail address differs: ${testUser.mail}", resultString)
    }

    @Test
    fun `checkUser of an disabled user with wrong status leads to status differs message`() {
        val testUser = generalServiceTest.createRandomTestUser(true)
        val testUserWrong = testUser.copy(disabled = true)
        val resultString = runBlocking { opService.checkUser(testUserWrong) }
        assertEquals("User status differs: active", resultString)
    }

    @Test
    fun `checkUser of an non existing user leads to not found message`() {
        val testUser = generalServiceTest.getRandomTestUser(true)
        val resultString = runBlocking { opService.checkUser(testUser) }
        assertEquals("Not found", resultString)
    }

    @Test
    fun `ChangeMail of an existing user leads to TRUE result`() {
        val testUser = generalServiceTest.createRandomTestUser(true)
        val result = opService.changeMailAddress(testUser, FakeData.generateMailAddress())
        assertTrue(result)
    }

    @Test
    fun `ChangeMail and check user leads to mail address differs differs message`() {
        val testUser = generalServiceTest.createRandomTestUser(true)
        val newMail = FakeData.generateMailAddress()
        opService.changeMailAddress(testUser, newMail)
        val resultString = runBlocking { opService.checkUser(testUser) }
        assertEquals("Mail address differs: $newMail", resultString)
    }

    @Test
    fun `ChangeMail of not existing user leads to FALSE`() {
        val testUser = generalServiceTest.getRandomTestUser(true)
        val result = opService.changeMailAddress(testUser, FakeData.generateMailAddress())
        assertFalse(result)
    }
}
