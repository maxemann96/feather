/*
 *    Copyright [2020-2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.multiservice

import dev.maximilian.feather.IControllableService
import dev.maximilian.feather.Permission
import dev.maximilian.feather.User
import dev.maximilian.feather.testutils.ServiceConfig
import dev.maximilian.feather.testutils.TestUser
import mu.KotlinLogging
import java.util.UUID

internal class ServiceTestUtilities(private val service: IControllableService) {
    private val logger = KotlinLogging.logger { }
    private val credentialProvider = ServiceConfig.ACCOUNT_CONTROLLER

    internal fun getRandomTestUser(active: Boolean): User =
        credentialProvider.createUser(TestUser.generateTestUser(permissions = setOf(Permission.USER), disabled = !active))

    internal fun createRandomTestUser(active: Boolean): User =
        service.createUser(getRandomTestUser(active), UUID.randomUUID()).apply {
            logger.info("Create test user successful.")
        }

    fun createSimpleUserTest(): Boolean =
        kotlin.runCatching {
            service.createUser(getRandomTestUser(true), UUID.randomUUID())
        }.onFailure {
            logger.error(it) { "Create test user failed!" }
        }.isSuccess
}
