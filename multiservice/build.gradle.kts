/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

plugins {
    alias(libs.plugins.kotlin.serialization)
}

tasks {
    test {
        maxParallelForks = (Runtime.getRuntime().availableProcessors() / 2).takeIf { it > 0 } ?: 1
    }
}

dependencies {
    implementation(project(":lib"))
    implementation(project(":authorization"))
    implementation(project(":openproject"))
    implementation(project(":nextcloud"))

    // Database ORM Framework
    implementation(libs.bundles.exposed)

    // Rest Client and JSON (De-)Serializer
    implementation(libs.bundles.jackson)

    // Rest framework
    implementation(libs.javalin.core)
    implementation(libs.javalin.openapi.core)
    kapt(libs.javalin.openapi.kapt)

    // Database driver implementation
    implementation(libs.postgres)

    // Redis driver
    implementation(libs.jedis)

    // LDAP driver
    implementation(libs.directory)

    // Text template compiler
    implementation(libs.mustache)

    // Mail framework
    implementation(libs.mail)

    // Im memory database
    testImplementation(libs.h2)

    // Http client for tests
    testImplementation(libs.bundles.unirest)
}
