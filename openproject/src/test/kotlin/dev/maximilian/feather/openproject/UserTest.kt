/*
 * Copyright [2021] Feather development team, see AUTHORS.md
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dev.maximilian.feather.openproject

import dev.maximilian.feather.openproject.api.IOpenProjectUserApi
import dev.maximilian.feather.openproject.api.overridePageSize
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.assertDoesNotThrow
import java.awt.image.BufferedImage
import javax.imageio.ImageIO
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertNull
import kotlin.test.assertTrue

class UserTest {
    private val api: IOpenProjectUserApi = TestUtil.openProject

    @Test
    fun `getAllUsers() not empty and contains admin user`() {
        val users = runBlocking { api.getUsers() }
        val adminUser =
            generateTestUser(
                firstName = "OpenProject",
                lastName = "Admin",
                email = "admin@example.net",
                admin = true,
            )
        val fetchedAdminUser = users.firstOrNull { it.login == "admin" }

        assertTrue("getAllUsers() should not return empty list") { users.isNotEmpty() }
        assertNotNull(fetchedAdminUser, "getAllUsers() should contain admin user")
        assertEquals(fetchedAdminUser.firstName, adminUser.firstName)
        assertEquals(fetchedAdminUser.lastName, adminUser.lastName)
        assertEquals(fetchedAdminUser.email, adminUser.email)
        assertEquals(fetchedAdminUser.admin, adminUser.admin)
        assertTrue("getAllUsers() should return correct admin user") {
            fetchedAdminUser.copy(id = 0) == adminUser
        }
    }

    @Test
    fun `getAllUsers() automatically pages the output and returns all users`() {
        runBlocking {
            val expectedSubSet = (0..25).map { async { api.createUser(generateTestUser(), "test123456") } }.awaitAll()
            overridePageSize = 10
            val actual = api.getUsers()
            overridePageSize = null
            val setMinus = expectedSubSet - actual.toSet()

            assertEquals(
                emptySet(),
                setMinus.toSet(),
                "Expected users minus all users should be empty" +
                    " (so expectedSubSet must be a real subset of all user OpenProject knows",
            )
        }
    }

    @Test
    fun `getUser() does return null on not existing id`() {
        assertDoesNotThrow {
            assertNull(runBlocking { api.getUser(-42) }, "getUser(NOT_EXISTENT_ID) should return null")
        }
    }

    @Test
    fun `createUser() with password works`() {
        val user = generateTestUser()
        val created = runBlocking { api.createUser(user, "test123456") }

        assertEquals(user, created.copy(id = 0))
    }

    @Test
    fun `create admin user works`() {
        val user = generateTestUser().copy(admin = true)
        val created = runBlocking { api.createUser(user, "test123456") }

        assertEquals(user, created.copy(id = 0))
    }

    @Test
    fun `createUser() with auth_source works`() {
        val user = generateTestUser()
        val created = runBlocking { api.createAuthSourceUser(user, 1) }

        assertEquals(user.copy(authSource = 1), created.copy(id = 0))
    }

    @Test
    fun `changeMailAddress() works`() {
        val user = generateTestUser()
        val created = runBlocking { api.createUser(user, "test123456") }

        val newMailAddress = generateMailAddress()
        val changed = runBlocking { api.changeMailAddress(created, newMailAddress) }

        assertEquals(user.copy(email = newMailAddress), changed.copy(id = 0))

        val repeatedGet = runBlocking { api.getUser(created.id) }

        assertNotNull(repeatedGet)
        assertEquals(user.copy(email = newMailAddress), repeatedGet.copy(id = 0))

        val byMail = runBlocking { api.getUserByEmail(newMailAddress) }

        assertNotNull(byMail)
        assertEquals(user.copy(email = newMailAddress), byMail.copy(id = 0))
    }

    @Test
    fun `changeUserName() works`() {
        val user = generateTestUser()
        val created = runBlocking { api.createUser(user, "test123456") }

        val newLastName = generateName()
        val changed = runBlocking { api.changeName(created, "Franz", newLastName) }

        assertEquals(
            user.copy(firstName = "Franz", lastName = newLastName),
            changed.copy(id = 0),
        )

        val repeatedGet = runBlocking { api.getUser(created.id) }

        assertNotNull(repeatedGet)
        assertEquals(
            user.copy(firstName = "Franz", lastName = newLastName),
            repeatedGet.copy(id = 0),
        )
    }

    @Test
    fun `deleteUser() works`() {
        runBlocking {
            val user = api.createUser(generateTestUser(), "1234567890")

            api.deleteUser(user)
            assertNull(api.getUser(user.id))
        }
    }

    @Test
    fun `getUserByLogin() returns the correct user`() {
        runBlocking {
            val user = api.createUser(generateTestUser(), "1234567890")
            val byLogin = api.getUserByLogin(user.login)

            assertEquals(user, byLogin)
        }
    }

    @Test
    fun `getUserByEmail() returns the correct user`() {
        runBlocking {
            val user = api.createUser(generateTestUser(), "1234567890")
            val byMail = api.getUserByEmail(user.email!!)

            assertEquals(user, byMail)
        }
    }

    @Test
    fun `lockUser() and unlockUser() works`() {
        runBlocking {
            val user = api.createUser(generateTestUser(), "1234567890")

            val user2 = api.getUser(user.id)
            assertNotNull(user2, "getUser should return non-null entry for test user")
            assertEquals(user2.status, "active")

            api.lockUser(user)

            val user3 = api.getUser(user.id)
            assertNotNull(user3, "getUser should return non-null entry for test user")
            assertEquals(user3.status, "locked")

            api.unlockUser(user)

            val user4 = api.getUser(user.id)
            assertNotNull(user4, "getUser should return non-null entry for test user")
            assertEquals(user4.status, "active")
        }
    }

    @Test
    fun `Remove empty user avatar returns success`() {
        runBlocking {
            val user = api.createUser(generateTestUser(), "1234567890")
            api.setUserAvatar(user, null)
        }
    }

    @Test
    fun `Set and delete user avatar is successful`() {
        runBlocking {
            val user = api.createUser(generateTestUser(), "1234567890")
            val avatar = ImageIO.read(UserTest::class.java.classLoader.getResourceAsStream("profile.png"))

            api.setUserAvatar(user, avatar)

            val scaledImage = ImageUtil.getToMaximumScaledImage(avatar)
            val retrievedUser = requireNotNull(api.getUser(user.id))
            val retrievedAvatar = retrievedUser.avatar.value

            assert(bufferedImagesEqual(retrievedAvatar, scaledImage))

            api.setUserAvatar(user, null)

            val againRetrievedUser = requireNotNull(api.getUser(user.id))

            assertNull(againRetrievedUser.avatar.value)
        }
    }

    private fun bufferedImagesEqual(img1: BufferedImage?, img2: BufferedImage?): Boolean {
        if (img1 == null && img2 == null) return true
        if (img1 == null || img2 == null) return false

        if (img1.width != img2.width || img1.height != img2.height) return false

        for (x in 0 until img1.width) {
            for (y in 0 until img1.height) {
                if (img1.getRGB(x, y) != img2.getRGB(x, y)) return false
            }
        }

        return true
    }
}
