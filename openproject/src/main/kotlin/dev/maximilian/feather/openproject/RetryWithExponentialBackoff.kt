package dev.maximilian.feather.openproject

import kotlinx.coroutines.delay
import java.time.Duration
import kotlin.math.pow

internal suspend fun <T> (suspend () -> T).retryWithExponentialBackoff(
    retries: Int,
    maxTime: Duration,
): T {
    require(retries > 0) { "At least one retry is needed, got $retries" }
    require(retries <= 30) { "At maximum 30 retries, got $retries" }
    require(maxTime > Duration.ZERO) { "Need a positive duration for exponential backoff, got $maxTime" }

    var currentDelay: Long = (maxTime.toMillis() / 2.0.pow(retries)).toLong()

    require(currentDelay > 0) { "The exponential factor is too big and leads to zero delay time in first step" }

    val delayFactor = 2
    var attempt = 0

    while (true) {
        try {
            return this()
        } catch (e: Exception) {
            attempt++

            if (attempt <= retries) {
                delay(currentDelay)
                currentDelay *= delayFactor
            } else {
                throw e
            }
        }
    }
}
