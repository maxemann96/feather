package dev.maximilian.feather.openproject.api

import io.github.z4kn4fein.semver.Version
import io.github.z4kn4fein.semver.toVersion
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.get

public interface IOpenProjectInfoApi {
    public suspend fun getOpenProjectVersion(): Version
}

internal class InfoApi(private val webpageClientFunction: () -> HttpClient, baseUrl: String) : IOpenProjectInfoApi {
    private val adminInfoUrl = "$baseUrl/admin/info"
    private val openProjectVersionRegex =
        Regex("OpenProject (0|[1-9]\\d*)\\.(0|[1-9]\\d*)\\.(0|[1-9]\\d*)(?:-((?:0|[1-9]\\d*|\\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\\.(?:0|[1-9]\\d*|\\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\\+([0-9a-zA-Z-]+(?:\\.[0-9a-zA-Z-]+)*))?")

    override suspend fun getOpenProjectVersion(): Version = webpageClientFunction().use { webpageClient ->
        webpageClient.get(adminInfoUrl).body<String>().let {
            openProjectVersionRegex.find(it)
        }.let {
            requireNotNull(it) { "Cannot find OpenProject version" }
        }.value.substring(12).toVersion()
    }
}
