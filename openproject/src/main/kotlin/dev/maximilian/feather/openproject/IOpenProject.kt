/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.openproject

import dev.maximilian.feather.openproject.api.IOpenProjectGroupApi
import dev.maximilian.feather.openproject.api.IOpenProjectInfoApi
import dev.maximilian.feather.openproject.api.IOpenProjectMembershipApi
import dev.maximilian.feather.openproject.api.IOpenProjectProjectApi
import dev.maximilian.feather.openproject.api.IOpenProjectRoleApi
import dev.maximilian.feather.openproject.api.IOpenProjectStatusApi
import dev.maximilian.feather.openproject.api.IOpenProjectUserApi
import dev.maximilian.feather.openproject.api.IOpenProjectWorkPackageApi
import java.io.Closeable

public interface IOpenProject :
    IOpenProjectRoleApi,
    IOpenProjectUserApi,
    IOpenProjectStatusApi,
    IOpenProjectProjectApi,
    IOpenProjectWorkPackageApi,
    IOpenProjectGroupApi,
    IOpenProjectMembershipApi,
    IOpenProjectInfoApi,
    Closeable
