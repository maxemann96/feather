package dev.maximilian.feather.iog

import dev.maximilian.feather.iog.internal.tools.PublicLinkConverter
import kotlin.test.Test
import kotlin.test.assertEquals

class PublicLinkConverterTest {
    @Test
    fun `Test real https conversion of internal to public link`() {
        val plc = PublicLinkConverter("https://nextcloud.apps.blabla.eu/apps/files/?dir=/IOG")
        val link = plc.convert("https://nextcloud.nextcloud.svc/s/pCpEcxN6r5nKZ55")
        assertEquals("https://nextcloud.apps.blabla.eu/s/pCpEcxN6r5nKZ55", link)
    }

    @Test
    fun `Test real http conversion of internal to public link`() {
        val plc = PublicLinkConverter("http://nextcloud.apps.blabla.eu/apps/files/?dir=/IOG")
        val link = plc.convert("https://nextcloud.nextcloud.svc/s/pCpEcxN6r5nKZ55")
        assertEquals("https://nextcloud.apps.blabla.eu/s/pCpEcxN6r5nKZ55", link)
    }

    @Test
    fun `Test conversion of internal to public link without http`() {
        val plc = PublicLinkConverter("nextcloud.apps.blabla.eu/apps/files/?dir=/IOG")
        val link = plc.convert("https://nextcloud.nextcloud.svc/s/pCpEcxN6r5nKZ55")
        assertEquals("https://nextcloud.apps.blabla.eu/s/pCpEcxN6r5nKZ55", link)
    }
}
