/*
 *    Copyright [2023] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

@file:Suppress("ktlint:standard:no-empty-file")

/*
package dev.maximilian.feather.iog.civicrm

import dev.maximilian.feather.iog.testframework.ApiTestUtilities
import dev.maximilian.feather.iog.testframework.CRMApiCalls
import dev.maximilian.feather.multiservice.BackgroundJobManager
import dev.maximilian.feather.testutils.ServiceConfig
import dev.maximilian.feather.testutils.TestUser
import kong.unirest.GenericType
import kong.unirest.HttpRequest
import kong.unirest.HttpResponse
import kotlinx.coroutines.runBlocking
import org.eclipse.jetty.http.HttpStatus
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

class AddUserToGroupCiviTest {
    @Test
    fun `Add user in civicrm also adds contact in CiviCRM`() {
        val credentials = ServiceConfig.CREDENTIAL_PROVIDER
        val backgroundJobManager = BackgroundJobManager(ApiTestUtilities.pool)
        val apiTestUtilities = ApiTestUtilities()
        apiTestUtilities.startIogPlugin(admin = true, backgroundJobManager = backgroundJobManager, true)
        apiTestUtilities.startMultiservice(backgroundJobManager)

        apiTestUtilities.createStandardGroups()
        apiTestUtilities.scenario!!.opUtilities.createStandardProjects()
        apiTestUtilities.createRGTest()
        apiTestUtilities.loginWithAdminUser()
        val testUser = ServiceConfig.CREDENTIAL_PROVIDER.getUserByUsername(TestUser.NORMAL_USER.username)
            ?: ServiceConfig.CREDENTIAL_PROVIDER.createUser(TestUser.NORMAL_USER)

        val t = credentials.getGroupByName("rg-test-crm")
        assertNotNull(t, "rg-test-crm was not created")

        val crm = CRMApiCalls(apiTestUtilities, backgroundJobManager)
        val response = crm.addUserToGroupByApi(t.id, testUser.id)

        runBlocking {
            assertEquals(HttpStatus.OK_200, response.status, "Http response status is not OK")
            val id = apiTestUtilities.scenario!!.civiCRM!!.civicrm.getContactIdByEmail(testUser.mail)

            assertNotNull(id, "Email was not generated in CiviCRM. Existing mails: ${apiTestUtilities.scenario!!.civiCRM!!.civicrm.getEmails().map { it.email + " of " + it.contactId }.joinToString()}")
            assertNotNull(
                apiTestUtilities.scenario!!.civiCRM!!.civicrm.getContact(id),
                "Contact was not generated in CiviCRM",
            )
            assertTrue(
                apiTestUtilities.scenario!!.civiCRM!!.civicrm.getGroupContactsByContactId(id).any { it.groupId == id },
                "GroupContact was not created",
            )
        }
        crm.deleteCMRSystemBlocking()
    }
    inline fun <reified B> HttpRequest<*>.asObject(): HttpResponse<B> =
        this.asObject(object : GenericType<B>() {})
}
*/
