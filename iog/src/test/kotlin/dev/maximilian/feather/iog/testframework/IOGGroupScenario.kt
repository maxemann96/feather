/*
 *    Copyright [2020-2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.testframework

import dev.maximilian.feather.Group
import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.iog.internal.settings.LdapNames
import dev.maximilian.feather.iog.settings.IogPluginConstants

class IOGGroupScenario(private val credentialProvider: ICredentialProvider) {
    private var ldapIDofInterestedGroup = 0
    private var ldapIDofIOGMember = 0
    private var ldapIDofTrialMembers = 0

    fun createBaseGroups() {
        val iPeopleGroup =
            Group(
                0,
                LdapNames.INTERESTED_PEOPLE,
                "Interested People of IOG",
                setOf(),
                setOf(),
                setOf(),
                setOf(),
                setOf(),
                setOf(),
            )
        ldapIDofInterestedGroup = credentialProvider.getGroupByName(LdapNames.INTERESTED_PEOPLE)?.id ?: credentialProvider.createGroup(iPeopleGroup).id

        val iogMemberGroup =
            Group(
                0,
                LdapNames.IOG_MEMBERS,
                "IOG Member",
                setOf(),
                setOf(),
                setOf(ldapIDofInterestedGroup),
                setOf(),
                setOf(),
                setOf(),
            )
        ldapIDofIOGMember = credentialProvider.getGroupByName(LdapNames.IOG_MEMBERS)?.id ?: credentialProvider.createGroup(iogMemberGroup).id

        val trialGroup =
            Group(
                0,
                LdapNames.TRIAL_MEMBERS,
                "Nichtfördermitglieder mit befristet erhöhten Rechten",
                setOf(),
                setOf(),
                setOf(ldapIDofInterestedGroup),
                setOf(),
                setOf(),
                setOf(),
            )
        ldapIDofTrialMembers = credentialProvider.getGroupByName(LdapNames.TRIAL_MEMBERS)?.id ?: credentialProvider.createGroup(trialGroup).id
    }

    // TODO this does not correctly model what the actual code does
    // This does not create the crm group
    // Remove this method and use the implementation, that is also used in the actual api request
    fun createRG(
        name: String,
        description: String,
    ) {
        val rgTest =
            Group(
                0,
                "Beschreibung von $description",
                name,
                setOf(),
                setOf(),
                setOf(),
                setOf(),
                setOf(),
                setOf(),
            )
        val id = credentialProvider.createGroup(rgTest).id

        val rgTestMemberName = "$name-member"
        val rgTestMember =
            Group(
                0,
                rgTestMemberName,
                "Beschreibung von $name-member",
                setOf(),
                setOf(),
                setOf(id, ldapIDofIOGMember),
                setOf(),
                setOf(),
                setOf(),
            )
        val memberID = credentialProvider.createGroup(rgTestMember).id

        val rgTestInterestedName = "$name-interested"
        val rgTestInterested =
            Group(
                0,
                rgTestInterestedName,
                "Beschreibung von $rgTestInterestedName",
                setOf(),
                setOf(),
                setOf(id, ldapIDofInterestedGroup),
                setOf(),
                setOf(),
                setOf(),
            )
        credentialProvider.createGroup(rgTestInterested)

        val rgTestAdminName = "$name-admin"
        val rgTestAdmin =
            Group(
                0,
                rgTestAdminName,
                "Beschreibung von $rgTestAdminName",
                setOf(),
                setOf(),
                setOf(id, memberID),
                setOf(),
                setOf(),
                setOf(),
            )
        credentialProvider.createGroup(rgTestAdmin)

        val rgTestTrialName = "$name-trial"
        val rgTestTrial =
            Group(
                0,
                rgTestTrialName,
                "Beschreibung von $rgTestTrialName",
                setOf(),
                setOf(),
                setOf(id, ldapIDofTrialMembers),
                setOf(),
                setOf(),
                setOf(),
            )
        credentialProvider.createGroup(rgTestTrial)
    }

    // TODO this does not correctly model what the actual code does
    // This does not create the trial group
    // Remove this method and use the implementation, that is also used in the actual api request
    fun createSimplePatternForBase(
        basename: String,
        description: String,
    ) {
        val kg =
            Group(
                0,
                basename,
                description,
                setOf(),
                setOf(),
                setOf(ldapIDofIOGMember),
                setOf(),
                setOf(),
                setOf(),
            )
        val id = credentialProvider.createGroup(kg).id

        val kgAdmin =
            Group(
                0,
                basename + IogPluginConstants.ADMIN_SUFFIX,
                "Administratoren der Gruppe $description",
                setOf(),
                setOf(),
                setOf(id),
                setOf(),
                setOf(),
                setOf(),
            )
        credentialProvider.createGroup(kgAdmin)
    }
}
