/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.group

import dev.maximilian.feather.User
import dev.maximilian.feather.iog.api.bindings.IogGroupDeleteRequest
import dev.maximilian.feather.iog.internal.settings.NextcloudFolders
import dev.maximilian.feather.iog.settings.IogPluginConstants
import dev.maximilian.feather.iog.testframework.ApiTestUtilities
import dev.maximilian.feather.multiservice.BackgroundJobManager
import dev.maximilian.feather.testutils.ServiceConfig
import dev.maximilian.feather.testutils.TestUser
import kong.unirest.core.RequestBodyEntity
import kotlinx.coroutines.runBlocking
import org.eclipse.jetty.http.HttpStatus
import org.junit.jupiter.api.TestInstance
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertNull

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class DeleteGroupTest {
    private val credentialProvider = ServiceConfig.CREDENTIAL_PROVIDER
    private val backgroundJobManager = BackgroundJobManager(ApiTestUtilities.pool)
    private val api = ApiTestUtilities()
    private var testUser: User?

    init {
        api.startIogPlugin(admin = true, backgroundJobManager = backgroundJobManager, true)
        api.createStandardGroups()
        api.scenario!!.opUtilities.createStandardProjects()
        testUser = credentialProvider.getUserByUsername(TestUser.ADMIN_USER.username)
    }

    @Test
    fun `Delete REGIONAL_GROUP as standard user returns FORBIDDEN`() {
        api.loginWithStandardUser()
        api.createRG("rg-test1", "RG Test1")
        val t = ServiceConfig.CREDENTIAL_PROVIDER.getGroupByName("rg-test1")!!
        val request = IogGroupDeleteRequest(TestUser.NORMAL_USER_PASSWORD, true)
        val res = deleteGroup(t.id, request).asEmpty()
        assertEquals(HttpStatus.FORBIDDEN_403, res.status)
    }

    @Test
    fun `Delete REGIONAL_GROUP as admin user returns OK and really deletes`() {
        api.loginWithAdminUser()
        api.createRG("rg-test2", "RG Test2")
        val id = "rg-test2"
        val t = credentialProvider.getGroupByName(id)!!
        val request = IogGroupDeleteRequest(TestUser.ADMIN_USER_PASSWORD, true)
        val res = deleteGroup(t.id, request).asEmpty()
        assertEquals(HttpStatus.CREATED_201, res.status)

        val folders = api.scenario!!.nextcloud!!.listFolders("${NextcloudFolders.GroupShare}/${NextcloudFolders.Projects}")
        assertFalse(folders.contains("RG Test2"), "Nextcloud folder was not deleted!")
        val testList =
            listOf(
                id,
                "$id${IogPluginConstants.MEMBER_SUFFIX}",
                "$id${IogPluginConstants.ADMIN_SUFFIX}",
                "$id${IogPluginConstants.INTERESTED_SUFFIX}",
            )
        runBlocking {
            assertNull(api.scenario!!.openProject!!.getGroupByName(id), "OpenProject $id project was not deleted")

            testList.forEach {
                assertNull(api.scenario!!.openProject!!.getGroupByName(it), "OpenProjectGroup $it was not deleted.")
            }
        }

        testList.forEach {
            assertNull(credentialProvider.getGroupByName(it), "Credential Group $it was not deleted.")
        }
    }

    private fun deleteGroup(
        id: Int,
        req: IogGroupDeleteRequest,
    ): RequestBodyEntity = api.restConnection.delete("${api.scenario!!.basePath}/bindings/multiservice/groups/$id").body(req)
}
