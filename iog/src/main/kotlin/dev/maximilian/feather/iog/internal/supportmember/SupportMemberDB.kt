/*
 *    Copyright [2020-2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.internal.supportmember

import dev.maximilian.feather.account.AccountController
import mu.KotlinLogging
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IdTable
import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.ReferenceOption
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.deleteAll
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.or
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.update
import org.jetbrains.exposed.sql.upsert

internal class SupportMemberDB(val db: Database, accountController: AccountController) {
    private val logger = KotlinLogging.logger { }
    private val dkpUserMapping = DKPUserMapping(accountController)
    private val metadata = SupportMemberMetadata()

    init {
        logger.info { "SupportMemberDB::init() create DB" }
        transaction(db) {
            SchemaUtils.createMissingTablesAndColumns(SupportMember, dkpUserMapping, metadata)
        }
        logger.info { "SupportMemberDB::init() finished" }
    }

    private class DKPUserMapping(accountController: AccountController) : Table() {
        val externalID: Column<EntityID<Int>> = reference("externalID", SupportMember.id, onDelete = ReferenceOption.CASCADE).uniqueIndex()
        val ldapID: Column<EntityID<Int>> = reference("ldapID", accountController.userIdColumn, onDelete = ReferenceOption.CASCADE)

        override val primaryKey: PrimaryKey = PrimaryKey(externalID, ldapID)
    }

    private object SupportMember : IdTable<Int>() {
        override val id: Column<EntityID<Int>> = integer("externalID").entityId()
        override val primaryKey = PrimaryKey(id)

        val nameBirthdateHash = varchar("nameBirthdateHash", 65)
        val mailHash1 = varchar("mailHash1", 65)
        val mailHash2 = varchar("mailHash2", 65)
    }

    private class SupportMemberMetadata : Table() {
        val key: Column<String> = varchar("key", 32)
        val value: Column<String> = varchar("value", 1024)
        override val primaryKey = PrimaryKey(key)
    }

    enum class MetadataKey(val dbKey: String) {
        LAST_UPDATED("last_updated"),
    }

    fun getMetadata(key: MetadataKey): String? =
        transaction(db) {
            metadata.select { metadata.key eq key.dbKey }.singleOrNull()?.get(metadata.value)
        }

    fun setMetadata(
        key: MetadataKey,
        value: String,
    ) = transaction(db) {
        metadata.select { metadata.key eq key.dbKey }.singleOrNull().let { existing ->
            if (existing != null) {
                metadata.update({ metadata.key eq key.dbKey }) { it[metadata.value] = value }
            } else {
                metadata.insert {
                    it[metadata.key] = key.dbKey
                    it[metadata.value] = value
                }
            }
        }
    }

    fun getExternalIDbyMail(mailHash: String): Int? =
        transaction(db) {
            SupportMember.select { (SupportMember.mailHash1 eq mailHash) or (SupportMember.mailHash2 eq mailHash) }.singleOrNull()?.get(
                SupportMember.id,
            )?.value
        }

    fun countSupportMember(): Long =
        transaction(db) {
            SupportMember.selectAll().count()
        }

    fun countRegisteredDKPUser(): Long =
        transaction(db) {
            dkpUserMapping.selectAll().count()
        }

    fun getExternalIDbyLDAP(ldapID: Int): Int? =
        transaction(db) {
            dkpUserMapping.select { dkpUserMapping.ldapID eq ldapID }.singleOrNull()?.get(dkpUserMapping.externalID)
        }?.value

    fun storeLdapAndExternalID(
        ldapID: Int,
        externalID: Int,
    ) = transaction(db) {
        dkpUserMapping.insert {
            it[dkpUserMapping.externalID] = externalID
            it[dkpUserMapping.ldapID] = ldapID
        }
    }

    fun countMailHash(mailHash: String): Int =
        transaction(db) {
            SupportMember.select { (SupportMember.mailHash1 eq mailHash) or (SupportMember.mailHash2 eq mailHash) }.count().toInt()
        }

    fun getExternalIDbyNameHash(thisNameBirthdateHash: String): Int? =
        transaction(db) {
            SupportMember.select { SupportMember.nameBirthdateHash eq thisNameBirthdateHash }.singleOrNull()?.get(
                SupportMember.id,
            )
        }?.value

    fun storeHash(
        externID: Int,
        nameBirthdayHash: String,
        mailHash: String,
        alternativeMailHash: String,
    ) = transaction(db) {
        SupportMember.upsert {
            it[id] = externID
            it[nameBirthdateHash] = nameBirthdayHash
            it[mailHash1] = mailHash
            it[mailHash2] = alternativeMailHash
        }
    }

    fun deleteAll() =
        transaction(db) {
            dkpUserMapping.deleteAll()
            metadata.deleteAll()
            SupportMember.deleteAll()
        }

    fun deleteSupportMembership(externalID: Int) =
        transaction(db) {
            dkpUserMapping.deleteWhere { dkpUserMapping.externalID eq externalID }
            SupportMember.deleteWhere { SupportMember.id eq externalID }
        }

    fun getAllRegisteredExternalID(): List<Int> =
        transaction(db) {
            dkpUserMapping.selectAll().orderBy(dkpUserMapping.externalID).map { it[dkpUserMapping.externalID].value }
        }

    fun getAllSupportMemberExternalID(): List<Int> =
        transaction(db) {
            SupportMember.selectAll().orderBy(SupportMember.id).map { it[SupportMember.id].value }
        }

    fun getAllRegistered(): List<Pair<Int, Int>> =
        transaction(db) {
            dkpUserMapping.selectAll().orderBy(
                dkpUserMapping.ldapID,
            ).map { Pair(it[dkpUserMapping.ldapID].value, it[dkpUserMapping.externalID].value) }
        }

    fun getAllSupportMember() =
        transaction(db) {
            SupportMember.selectAll().orderBy(SupportMember.id).map {
                SupportMember(
                    it[SupportMember.id].value,
                    it[SupportMember.nameBirthdateHash],
                    it[SupportMember.mailHash1],
                    it[SupportMember.mailHash2],
                )
            }
        }

    fun deleteUserRegistration(ldapID: Int) =
        transaction(db) {
            dkpUserMapping.deleteWhere { dkpUserMapping.ldapID eq ldapID }
        }
}
