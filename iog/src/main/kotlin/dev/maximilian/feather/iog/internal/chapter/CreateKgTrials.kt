/*
 *    Copyright [2023] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.internal.chapter

import dev.maximilian.feather.Group
import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.iog.internal.group.CreateGroupConfig
import dev.maximilian.feather.iog.internal.group.plausibility.OpenProjectPlausibility
import dev.maximilian.feather.iog.internal.groupPattern.MemberAdminTrialPattern
import dev.maximilian.feather.iog.internal.settings.LdapNames
import dev.maximilian.feather.iog.internal.settings.NextcloudFolders
import dev.maximilian.feather.iog.internal.tools.OpenProjectPCreator
import dev.maximilian.feather.iog.settings.GroupKind
import dev.maximilian.feather.iog.settings.IogPluginConstants
import dev.maximilian.feather.iog.settings.OPNameConfig
import dev.maximilian.feather.multiservice.BackgroundJobManager
import dev.maximilian.feather.multiservice.events.GroupSynchronizationEvent
import dev.maximilian.feather.multiservice.openproject.OpenProjectService
import dev.maximilian.feather.nextcloud.Nextcloud
import dev.maximilian.feather.nextcloud.groupfolders.entities.GroupFolderDetails
import dev.maximilian.feather.nextcloud.ocs.general.PermissionType
import mu.KLogging
import java.util.UUID

internal data class PreconditionResult(
    val groupFolderDetails: GroupFolderDetails?,
    val errorMessages: List<String>,
)

internal data class TrialOperationResult(
    val status: String,
    val errorMessages: List<String>,
)

internal class CreateKgTrials(
    private val credentials: ICredentialProvider,
    val backgroundJobManager: BackgroundJobManager,
    private val openProjectService: OpenProjectService,
    private val syncEvent: GroupSynchronizationEvent,
    private val onc: OPNameConfig,
    private val nc: Nextcloud,
    private val nextcloudPublicURL: String,
) {
    var groups: Collection<Group> = emptyList()
    companion object : KLogging()

    suspend fun createKgTrialGroups(
        jobId: UUID,
        autoRepair: Boolean,
    ): TrialOperationResult {
        val errorMessages = mutableListOf<String>()
        var status = "NOK"
        logger.info { "CreateKgTrial::createKgTrialGroups fetching all groups " }
        backgroundJobManager.setJobStatus(jobId, "Fetching all users and groups from LDAP")

        val userAndGroups = credentials.getUsersAndGroups()
        groups = userAndGroups.second
        backgroundJobManager.setJobStatus(jobId, "Check read OpenProject projects and memberships")
        logger.info { "CreateKgTrial::createKgTrialGroups create OPC" }
        val opc = OpenProjectPCreator(openProjectService.openproject, onc, jobId, backgroundJobManager)
        val opp = OpenProjectPlausibility(openProjectService.openproject, credentials, opc, onc, userAndGroups.first)

        logger.info { "CreateKgTrial::createKgTrialGroups check preconditions" }
        backgroundJobManager.setJobStatus(jobId, "Check preconditions")

        val cachedResults = checkPreconditions(opp.getProjects().map { it -> it.identifier })
        if (cachedResults.errorMessages.isNotEmpty()) {
            errorMessages.addAll(cachedResults.errorMessages)
            backgroundJobManager.setJobStatus(jobId, "Prüfung mit Fehlern abgeschlossen")
        } else if (autoRepair) {
            backgroundJobManager.setJobStatus(jobId, "Add kg trial groups")
            logger.warn { "CreateKgTrial::createKgTrialGroups convert" }
            createKgTrialGroups(cachedResults, opc, opp)
            backgroundJobManager.setJobStatus(jobId, "Die KG-Trial-Konvertierung wurde erfolgreich durchgeführt.")
            status = "OK"
        } else {
            backgroundJobManager.setJobStatus(jobId, "Vorbedingungen für eine KG-Trial-Konvertierung sind erfüllt.")
            status = "OK"
        }
        logger.warn { "CreateKgTrial::createKgTrialGroups status: $status result: ${errorMessages.joinToString(". ") }" }
        return TrialOperationResult(status, errorMessages)
    }

    private suspend fun checkPreconditions(projectNames: List<String>): PreconditionResult {
        val errorMessages = mutableListOf<String>()

        if (credentials.getGroupByName(LdapNames.IOG_MEMBERS) == null) errorMessages += "Gruppe '${LdapNames.IOG_MEMBERS}' existiert nicht"
        if (credentials.getGroupByName(LdapNames.INTERESTED_PEOPLE) == null) errorMessages += "Gruppe '${LdapNames.INTERESTED_PEOPLE}' existiert nicht"

        if (!openProjectService.openproject.getRoles().any { it.name == onc.memberRoleName }) {
            errorMessages += "OpenProject enthält keine Memberrolle (${onc.memberRoleName})"
        }

        groups.filter { it.name.startsWith(IogPluginConstants.KG_PREFIX) && it.name.endsWith(IogPluginConstants.ADMIN_SUFFIX) }.forEach {
            val groupNameWithoutPrefix = it.name.removeSuffix(IogPluginConstants.ADMIN_SUFFIX)
            if (credentials.getGroupByName(groupNameWithoutPrefix) == null) {
                errorMessages += "Konnte Basisgruppe für $groupNameWithoutPrefix nicht finden. Gebildet aus ${it.name}"
            }
            if (credentials.getGroupByName(groupNameWithoutPrefix + IogPluginConstants.ADMIN_SUFFIX) == null) {
                errorMessages += "Admin Gruppe <${groupNameWithoutPrefix + IogPluginConstants.ADMIN_SUFFIX} existiert nicht!"
            }
            if (!projectNames.any { name -> name == groupNameWithoutPrefix }) {
                errorMessages += "Konnte kein Projekt zur Gruppe ${it.name} finden."
            }
        }
        val gf = nc.findGroupFolder(NextcloudFolders.GroupShare)
        if (gf == null) errorMessages += "Groupfolder ${NextcloudFolders.GroupShare} existiert nicht."
        return PreconditionResult(gf, errorMessages)
    }

    private suspend fun createKgTrialGroups(
        g: PreconditionResult,
        opc: OpenProjectPCreator,
        opPlausibility: OpenProjectPlausibility,
    ) {
        val gf = g.groupFolderDetails!!
        val interested = credentials.getGroupByName(LdapNames.INTERESTED_PEOPLE)!!
        val roles = openProjectService.openproject.getRoles()
        val memberRole = roles.find { it.name == onc.memberRoleName }!!
        val generalTrialGroupLdap =
            credentials.getGroupByName(LdapNames.TRIAL_MEMBERS) ?: credentials.createGroup(
                Group(
                    0, LdapNames.TRIAL_MEMBERS, "Nichtfördermitglieder mit befristet erhöhten Rechten",
                    emptySet(), emptySet(), setOf(interested.id), emptySet(), emptySet(), emptySet(),
                ),
            )
        opc.getOrCreateGeneralGroup(generalTrialGroupLdap, syncEvent)

        val allNC = gf.groups?.mapNotNull { it.key } ?: emptyList()
        val ids = mutableListOf<String>()
        groups.filter { it.name.startsWith(IogPluginConstants.KG_PREFIX) && it.name.endsWith(IogPluginConstants.ADMIN_SUFFIX) }.forEach {
            val pat = MemberAdminTrialPattern(credentials)
            val groupNameWithoutPrefix = it.name.removeSuffix(IogPluginConstants.ADMIN_SUFFIX)
            ids.add(groupNameWithoutPrefix)

            val opDescription = opPlausibility.getDescription(onc.credentialGroupToOpenProjectGroup(groupNameWithoutPrefix))
            val gk = GroupKind.COMPETENCE_GROUP
            val trialOfKgGroup =
                pat.addTrialGroupIfNecessary(
                    CreateGroupConfig(groupNameWithoutPrefix, opDescription, emptySet(), gk, nextcloudPublicURL),
                )

            opc.addSingleGroup(
                trialOfKgGroup,
                opPlausibility.getProjects().first { it.identifier == groupNameWithoutPrefix },
                memberRole,
                syncEvent,
            )
            credentials.getGroupByName(
                trialOfKgGroup.name.removeSuffix(IogPluginConstants.TRIAL_SUFFIX).plus(IogPluginConstants.MEMBER_SUFFIX),
            )?.let { member ->
                opc.addSingleGroup(
                    member,
                    opPlausibility.getProjects().first { it.identifier == groupNameWithoutPrefix },
                    memberRole,
                    syncEvent,
                )
            }

            if (!allNC.contains(trialOfKgGroup.name)) {
                nc.addGroupToGroupFolder(
                    gf,
                    trialOfKgGroup.name,
                    setOf(PermissionType.Read, PermissionType.Update, PermissionType.Create, PermissionType.Delete),
                )
                nc.addGroupToGroupFolder(
                    gf,
                    groupNameWithoutPrefix.plus(IogPluginConstants.MEMBER_SUFFIX),
                    setOf(PermissionType.Read, PermissionType.Update, PermissionType.Create, PermissionType.Delete),
                )
            }
        }
    }
}
