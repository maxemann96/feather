/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.internal.tools

import dev.maximilian.feather.iog.internal.group.MembershipAssociation
import dev.maximilian.feather.iog.internal.group.MembershipAssociationArguments
import dev.maximilian.feather.iog.internal.group.OpenProjectDetails
import dev.maximilian.feather.iog.internal.group.OpenProjectProjectSchema
import dev.maximilian.feather.iog.settings.IogPluginConstants
import dev.maximilian.feather.iog.settings.OPNameConfig
import dev.maximilian.feather.multiservice.BackgroundJobManager
import dev.maximilian.feather.openproject.IOpenProject
import dev.maximilian.feather.openproject.OpenProjectMembership
import dev.maximilian.feather.openproject.OpenProjectProject
import kotlinx.coroutines.runBlocking
import mu.KLogging
import java.util.Locale
import java.util.UUID

internal data class SandboxDetails(
    val sandboxDetails: OpenProjectDetails?,
    val templateDetails: OpenProjectDetails,
)

internal class ResetSandbox(
    private val onc: OPNameConfig,
    private val openproject: IOpenProject?,
    private val backgroundJobManager: BackgroundJobManager,
) {
    companion object : KLogging()

    fun getSandboxStatus(): SandboxDetails {
        if (openproject == null) {
            throw RuntimeException("Cannot use Openproject")
        }

        val openProjectStatus = runBlocking { openproject.getStatus() }
        val templateProject = runBlocking { openproject.getProjectByIdentifierOrName(onc.projectTemplate) }
        if (templateProject == null) {
            logger.error { "getSandboxStatus Template project ${onc.projectTemplate} not found." }
            throw RuntimeException("Template project ${onc.projectTemplate} not found")
        }

        val templateWorkPackages = runBlocking { openproject.getWorkPackagesOfProject(templateProject, openProjectStatus) }
        logger.info {
            "getSandboxStatus #WP of template (project id: ${templateProject.id}): ${templateWorkPackages.size}."
        }

        val templateStatusList = templateWorkPackages.map { it.status.name }

        val templateWorkPackageStats =
            templateStatusList.associateWith {
                templateWorkPackages.filter { e ->
                    e.status.name == it
                }.size
            }

        val templateDetails =
            OpenProjectDetails(
                emptyList(),
                OpenProjectProjectSchema(templateProject.id, templateProject.name, templateProject.identifier, templateProject.description),
                templateWorkPackageStats,
            )

        val sandboxProject = runBlocking { openproject.getProjectByIdentifierOrName(onc.sandboxId) }
        val sandboxDetails =
            if (sandboxProject == null) {
                logger.error { "getSandboxStatus Sandbox project ${onc.sandboxId} not found." }
                null
            } else {
                val sandboxWorkPackages = runBlocking { openproject.getWorkPackagesOfProject(sandboxProject, openProjectStatus) }
                logger.info {
                    "getSandboxStatus #WP of sandbox (project id: ${sandboxProject.id}): ${sandboxWorkPackages.size}."
                }

                val sandboxStatusList =
                    sandboxWorkPackages.map {
                        it.status.name
                    }

                val sandboxWorkPackageStats =
                    sandboxStatusList.associateWith {
                        sandboxWorkPackages.filter { e ->
                            e.status.name == it
                        }.size
                    }

                OpenProjectDetails(
                    emptyList(),
                    OpenProjectProjectSchema(sandboxProject.id, sandboxProject.name, sandboxProject.identifier, sandboxProject.description),
                    sandboxWorkPackageStats,
                )
            }

        return SandboxDetails(sandboxDetails, templateDetails)
    }

    suspend fun resetSandbox(jobId: UUID?) {
        if (openproject == null) {
            throw RuntimeException("Cannot use Openproject")
        }

        jobId?.let { backgroundJobManager.setJobStatus(it, "Deleting old sandbox project") }

        var sandboxName =
            onc.sandboxId.replaceFirstChar { if (it.isLowerCase()) it.titlecase(Locale.getDefault()) else it.toString() }
        val sandboxProject = openproject.getProjectByIdentifierOrName(onc.sandboxId)
        if (sandboxProject != null) {
            sandboxName = sandboxProject.name
            openproject.deleteProject(sandboxProject)
        }

        jobId?.let { backgroundJobManager.setJobStatus(it, "Preparing membership assignment") }

        val roles = openproject.getRoles()

        // membership roles for sandbox
        val rawRolesToBeAssigned: MutableList<MembershipAssociationArguments> =
            mutableListOf(
                MembershipAssociationArguments(
                    onc.readerRoleName,
                    onc.interestedPeopleGroupName,
                ),
                MembershipAssociationArguments(
                    onc.memberRoleName,
                    onc.iogMembersGroupName,
                ),
                MembershipAssociationArguments(
                    onc.projectAdminRoleName,
                    onc.kgItGroupName,
                ),
                MembershipAssociationArguments(
                    onc.memberRoleName,
                    onc.trialMemberGroupName,
                ),
            )
        // add project-admin membership for every node admin
        /*CacheManager.getGroups().filter{ it.name.endsWith(MultiServiceIogPluginConstants.admin_suffix) }
            .forEach{
                rawRolesToBeAssigned.add(MembershipAssociationArguments(projectAdminRoleName, it.name))
            }*/
        val allGroups = openproject.getGroups()
        allGroups.filter { it.name.lowercase(Locale.getDefault()).endsWith(IogPluginConstants.ADMIN_SUFFIX) }
            .forEach {
                rawRolesToBeAssigned.add(
                    MembershipAssociationArguments(
                        onc.projectAdminRoleName,
                        it.name,
                    ),
                )
            }

        val rolesToBeAssigned: List<MembershipAssociation> =
            rawRolesToBeAssigned.map { args ->
                val role = roles.find { it.name == args.roleName }
                val group = allGroups.firstOrNull { it.name == args.groupName }
                if (role == null) {
                    throw IllegalStateException("Could not find relevant openproject role (${args.roleName})")
                } else if (group == null) {
                    throw IllegalStateException("Could not find relevant openproject group (${args.groupName})")
                }
                MembershipAssociation(role, group)
            }

        jobId?.let { backgroundJobManager.setJobStatus(it, "Cloning project template into new sandbox") }

        // Clone template groups in OpenProject
        val newProject: OpenProjectProject?
        try {
            val templateProject = openproject.getProjectByIdentifierOrName(onc.projectTemplate)

            requireNotNull(templateProject) { "Template project ${onc.projectTemplate}" }

            newProject =
                openproject.cloneProject(
                    templateProject,
                    sandboxName,
                    // copy description from template
                    null,
                    onc.stuffToBeClonedFromTemplate,
                    false,
                    sandboxProject?.parent?.value,
                )
        } catch (e: Exception) {
            logger.error(e) { "resetSandbox Cloning template project ${onc.projectTemplate} in OpenProject not successful." }
            throw RuntimeException("Failed to clone template project in OpenProject")
        }

        jobId?.let { backgroundJobManager.setJobStatus(it, "Registering memberships") }

        // Register project membership with appropriate roles in OpenProject
        try {
            rolesToBeAssigned.forEach {
                openproject.createMembership(
                    OpenProjectMembership(
                        0,
                        newProject,
                        it.group,
                        listOf(it.role),
                    ),
                )
            }
        } catch (e: Exception) {
            logger.error(e) { "resetSandbox Project membership registration in OpenProject not successful." }
            throw RuntimeException("Failed to register project membership in OpenProject")
        }
    }
}
