/*
 *    Copyright [2020-2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.api

import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.Permission
import dev.maximilian.feather.iog.api.bindings.ChapterDetailsRequest
import dev.maximilian.feather.iog.internal.chapter.ChapterDB
import dev.maximilian.feather.iog.internal.chapter.CreateChapterDB
import dev.maximilian.feather.iog.internal.chapter.GetChapterDetails
import dev.maximilian.feather.iog.internal.group.IogGroupSchema
import dev.maximilian.feather.iog.settings.OPNameConfig
import dev.maximilian.feather.multiservice.BackgroundJobManager
import dev.maximilian.feather.multiservice.openproject.OpenProjectService
import dev.maximilian.feather.session
import io.javalin.Javalin
import io.javalin.apibuilder.ApiBuilder
import io.javalin.http.BadRequestResponse
import io.javalin.http.Context
import io.javalin.http.ForbiddenResponse
import io.javalin.http.HttpStatus
import io.javalin.http.bodyAsClass
import io.javalin.openapi.HttpMethod
import io.javalin.openapi.OpenApi
import io.javalin.openapi.OpenApiContent
import io.javalin.openapi.OpenApiRequestBody
import io.javalin.openapi.OpenApiResponse
import mu.KotlinLogging
import java.util.*

internal class IogChapterApi(
    app: Javalin,
    val credentialProvider: ICredentialProvider,
    val backgroundJobManager: BackgroundJobManager,
    private val openProjectService: OpenProjectService,
    private val onc: OPNameConfig,
    private val chapterDB: ChapterDB,
    private val iogGroupSchema: IogGroupSchema,
) {
    init {
        app.routes {
            ApiBuilder.path("bindings/iog/chapter") {
                ApiBuilder.post("/db", ::initChapterDB, Permission.ADMIN)
                ApiBuilder.get("/db", ::getAllChapters, Permission.ADMIN)
                ApiBuilder.post("/details", ::getChapterDetails, Permission.ADMIN)
            }
        }
    }

    private val logger = KotlinLogging.logger {}

    @OpenApi(
        summary = "Introduce chapter database to register all descriptions of chapter",
        requestBody = OpenApiRequestBody([OpenApiContent(Unit::class)]),
        responses = [OpenApiResponse("201"), OpenApiResponse("400"), OpenApiResponse("403")],
        path = "/v1/bindings/iog/chapter/db",
        methods = [HttpMethod.POST],
    )
    private fun initChapterDB(ctx: Context) {
        val session = ctx.session()

        val creator = session.user
        if (!creator.permissions.contains(Permission.ADMIN)) {
            logger.warn { "IogChapterApi::initChapterDB() permission denied. Admin required." }
            throw ForbiddenResponse()
        }

        val simulate: Boolean
        try {
            simulate = ctx.bodyAsClass<Boolean>()
        } catch (e: Exception) {
            logger.warn { "IogChapterApi::initChapterDB() malformed entity" }
            throw BadRequestResponse()
        }
        // Sanity checks, TODO json schema is a more better way to check on this!
        val errorMessages = mutableListOf<String>()

        if (errorMessages.isNotEmpty()) {
            logger.warn { "Will not init chapter db by admin ${creator.displayName} due to errors: $errorMessages" }

            ctx.status(HttpStatus.BAD_REQUEST)
            ctx.json(errorMessages)
        } else {
            if (simulate) {
                logger.info { "About to simulate chapter db filling by admin ${creator.displayName}" }
            } else {
                logger.info { "About to init chapter db by admin ${creator.displayName}" }
            }

            val t =
                CreateChapterDB(
                    backgroundJobManager,
                    credentialProvider,
                    chapterDB,
                    iogGroupSchema,
                    onc,
                    openProjectService.openproject,
                )

            try {
                val newJob =
                    backgroundJobManager.runBackgroundJob(
                        t::createChapterDB,
                        simulate,
                    )

                ctx.status(HttpStatus.CREATED)
                ctx.json(newJob)
            } catch (e: Exception) {
                logger.warn(e) { "Will process chapter db filling by admin ${creator.displayName} due to exception" }

                errorMessages += "Interner Fehler (Siehe Feather Logs)"
                ctx.status(HttpStatus.BAD_REQUEST)
                ctx.json(errorMessages)
                return
            }
        }
    }

    @OpenApi(
        summary = "Get details of a given chapter",
        requestBody = OpenApiRequestBody([OpenApiContent(Unit::class)]),
        responses = [OpenApiResponse("200"), OpenApiResponse("400"), OpenApiResponse("403")],
        path = "/v1/bindings/iog/chapter/db",
        methods = [HttpMethod.GET],
    )
    private fun getChapterDetails(ctx: Context) {
        val session = ctx.session()
        val viewer = session.user
        if (!viewer.permissions.contains(Permission.ADMIN)) {
            logger.warn { "IogChapterApi::getChapterDetails() permission denied. Admin required" }
            throw ForbiddenResponse()
        }

        val body: ChapterDetailsRequest
        try {
            body = ctx.bodyAsClass<ChapterDetailsRequest>()
        } catch (e: Exception) {
            logger.warn { "IogChapterApi::getChapterDetails() malformed entity" }
            throw BadRequestResponse()
        }
        // Sanity checks, TODO json schema is a more better way to check on this!
        val errorMessages = mutableListOf<String>()
        if (body.chapters.isEmpty()) errorMessages += "Keine Struktureinheiten angegeben"

        if (errorMessages.isNotEmpty()) {
            logger.warn { "Will not get chapter details due to errors: $errorMessages" }

            ctx.status(org.eclipse.jetty.http.HttpStatus.BAD_REQUEST_400)
            ctx.json(errorMessages)
        } else {
            val getChapterDetailsService = GetChapterDetails(chapterDB)

            val chapterDetails =
                body.chapters.mapNotNull {
                    logger.info { "About to get details for chapter $it" }

                    try {
                        getChapterDetailsService.getChapterDetails(UUID.fromString(it))
                    } catch (e: Exception) {
                        logger.warn(e) { "Could not get details of chapter $it due to exception" }

                        errorMessages += "Interner Fehler (Benachrichtige die IT)"

                        null
                    }
                }

            if (errorMessages.isNotEmpty()) {
                ctx.status(org.eclipse.jetty.http.HttpStatus.BAD_REQUEST_400)
                ctx.json(errorMessages)
            } else {
                ctx.status(org.eclipse.jetty.http.HttpStatus.OK_200)
                ctx.json(chapterDetails)
            }
        }
    }

    @OpenApi(
        summary = "List all registered chapters",
        requestBody = OpenApiRequestBody([OpenApiContent(Unit::class)]),
        responses = [OpenApiResponse("200"), OpenApiResponse("403")],
        path = "/v1/bindings/iog/chapter/db",
        methods = [HttpMethod.GET],
    )
    private fun getAllChapters(ctx: Context) {
        ctx.session()
        ctx.status(200)
        ctx.json(chapterDB.getAllChapterIDs())
    }
}
