/*
 *    Copyright [2023] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.internal.civicrm

import dev.maximilian.feather.Group
import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.civicrm.CiviCRMConstants
import dev.maximilian.feather.civicrm.internal.ICiviCRM
import dev.maximilian.feather.civicrm.internal.api.ACLProtocol
import dev.maximilian.feather.iog.internal.settings.CiviCRMNames
import dev.maximilian.feather.iog.internal.settings.LdapNames
import dev.maximilian.feather.iog.settings.IogPluginConstants
import kotlinx.coroutines.runBlocking
import mu.KLogging

class CiviGroupScheme(private val credentials: ICredentialProvider, private val civicrm: ICiviCRM) {
    private companion object : KLogging()
    fun createStandardGroups(): CiviGroupSchemeCreationResult {
        val memberGroupId = createMemberGroup()

        var userGroupID: Int
        var ethicGroupId: Int?
        var centralOfficeGroupId: Int
        var associationBoardId: Int
        var personContacts: dev.maximilian.feather.civicrm.entities.Group
        var orgaContacts: dev.maximilian.feather.civicrm.entities.Group
        var contactMainGroup: dev.maximilian.feather.civicrm.entities.Group
        runBlocking {
            userGroupID = createUsersGroup()

            val allContactTypes = civicrm.getContactTypes()
            IogPluginConstants.CiviCRMCustomType.values().forEach { e ->
                allContactTypes.singleOrNull { it.name == e.protocolName }?.let { logger.info { "Contact type ${it.name} exists. No creation." } }
                    ?: civicrm.createContactType(e.protocolName, e.label, e.parent)
            }

            contactMainGroup =
                civicrm.getGroupByName(CiviCRMNames.CONTACT_GROUP_NAME)
                    ?: civicrm.createGroup(CiviCRMNames.CONTACT_GROUP_NAME, "Personen- und Organisationenkontakte von Spendern")

            orgaContacts =
                civicrm.getGroupByName(CiviCRMNames.ORGANIZATION_GROUP_NAME)
                    ?: civicrm.createGroup(CiviCRMNames.ORGANIZATION_GROUP_NAME, "Spenderorganisationen")

            personContacts =
                civicrm.getGroupByName(CiviCRMNames.PERSON_GROUP_NAME)
                    ?: civicrm.createGroup(CiviCRMNames.PERSON_GROUP_NAME, "Kontaktpersonen")

            civicrm.updateParentOfGroup(orgaContacts.id, contactMainGroup.id)
            civicrm.updateParentOfGroup(personContacts.id, contactMainGroup.id)

            ethicGroupId = createEthicGroup(userGroupID, civicrm)
            centralOfficeGroupId = createContactAdminGroup(
                LdapNames.CENTRAL_OFFICE,
                "Geschäftsstelle",
                IogPluginConstants.CiviCRMCustomType.IogUser,
                userGroupID,
                contactMainGroup.id,
            )
            associationBoardId = createContactAdminGroup(
                LdapNames.ASSOCIATION_BOARD,
                "Vorstand",
                IogPluginConstants.CiviCRMCustomType.IogUser,
                userGroupID,
                contactMainGroup.id,
            )
        }
        return CiviGroupSchemeCreationResult(
            memberGroupId,
            userGroupID,
            ethicGroupId,
            centralOfficeGroupId,
            associationBoardId,
            personContacts.id,
            orgaContacts.id,
            contactMainGroup.id,
        )
    }

    internal fun createSingleCiviChapter(
        credentialGroup: Group,
        userGroupId: Int,
        orgaContacts: Int,
        personContacts: Int,
        contactAddedTriggerId: Int,
        addContactActionId: Int,
        contactCreatedByConditionId: Int,
        contactHasSubTypeConditionId: Int,
        updateRules: Boolean,
        simpleDescription: String,
    ) {
        logger.info("CiviGroupScheme::createSingleCiviGroup create group for ${credentialGroup.name} in civicrm")

        runBlocking {
            val result =
                civicrm.createOrReplaceSubgroupWithACLPermission(
                    credentialGroup.name,
                    credentialGroup.description,
                    "Rolle für ${credentialGroup.name}",
                    CiviCRMNames.CRM_USER_ROLE_NAME,
                )
            civicrm.updateParentOfGroup(result.group.id, userGroupId)

            val contactsCreated = civicrm.getGroupByName(credentialGroup.name + IogPluginConstants.CONTACT_SUFFIX)
                ?: civicrm.createGroup(
                    credentialGroup.name + IogPluginConstants.CONTACT_SUFFIX,
                    "Personenkontakte der " + credentialGroup.description,
                )

            val organizationsCreated = civicrm.getGroupByName(credentialGroup.name + IogPluginConstants.ORGA_SUFFIX)
                ?: civicrm.createGroup(
                    credentialGroup.name + IogPluginConstants.ORGA_SUFFIX,
                    "Organisationen der " + credentialGroup.description,
                )
            civicrm.updateParentOfGroup(contactsCreated.id, personContacts)
            civicrm.updateParentOfGroup(organizationsCreated.id, orgaContacts)

            civicrm.createACL(
                "Von " + credentialGroup.name + " verwaltete Personenkontakte",
                result.roleIndex,
                contactsCreated.id,
                ACLProtocol.Operations.EDIT,
            )
            civicrm.createACL(
                "Von " + credentialGroup.name + " verwaltete Organisationen",
                result.roleIndex,
                organizationsCreated.id,
                ACLProtocol.Operations.EDIT,
            )
            civicrm.createACL(
                "Selbstverwaltung von " + credentialGroup.name + " für eigene Aktivitäten",
                result.roleIndex,
                result.group.id,
                ACLProtocol.Operations.EDIT,
            )

            val personRule = civicrm.createCiviRulesRule(
                "${credentialGroup.name}-person-rule",
                "$simpleDescription Kontaktperson hinzufügen",
                contactAddedTriggerId,
                "Ordnet den von den Nutzern der $simpleDescription hinzugefügten Kontakten die korrekte Gruppe zu",
                "Von ${credentialGroup.description} hinzugefügte Personenkontakte sollen in ${contactsCreated.name} hinzugefügt werden",
                "Contact contacts are added",
            )
            civicrm.createCiviRulesRuleAction(
                personRule.id,
                addContactActionId,
                civicrm.buildActionParamStringToAddContactToGroup(contactsCreated.id),
            )
            civicrm.createCiviRulesRuleCondition(
                personRule.id,
                contactCreatedByConditionId,
                null,
                civicrm.buildIsInOneOfSelectedGroupCondition(result.group.id, false),
            )
            civicrm.createCiviRulesRuleCondition(
                personRule.id,
                contactHasSubTypeConditionId,
                "AND",
                civicrm.buildIsOneOfTypeCondition(
                    listOf(IogPluginConstants.CiviCRMCustomType.ContactPerson.protocolName),
                ),
            )

            val orgaRule = civicrm.createCiviRulesRule(
                "${credentialGroup.name}-orga-rule",
                "$simpleDescription Organisation hinzufügen",
                contactAddedTriggerId,
                "Ordnet den von den Nutzern von $simpleDescription hinzugefügten Organisationen die korrekte Gruppe zu",
                "Von ${credentialGroup.description} hinzugefügte Organisationen sollen in ${organizationsCreated.name} hinzugefügt werden",
                "Orga contacts are added",
            )
            civicrm.createCiviRulesRuleAction(
                orgaRule.id,
                addContactActionId,
                civicrm.buildActionParamStringToAddContactToGroup(organizationsCreated.id),
            )

            civicrm.createCiviRulesRuleCondition(
                orgaRule.id,
                contactCreatedByConditionId,
                null,
                civicrm.buildIsInOneOfSelectedGroupCondition(result.group.id, false),
            )
            civicrm.createCiviRulesRuleCondition(
                orgaRule.id,
                contactHasSubTypeConditionId,
                "AND",
                civicrm.buildIsOneOfTypeCondition(
                    listOf(
                        IogPluginConstants.CiviCRMCustomType.Foundation.protocolName,
                        IogPluginConstants.CiviCRMCustomType.Company.protocolName,
                        IogPluginConstants.CiviCRMCustomType.Association.protocolName,
                        IogPluginConstants.CiviCRMCustomType.DefaultOrganization.protocolName,
                        IogPluginConstants.CiviCRMCustomType.MediaCompany.protocolName,
                        IogPluginConstants.CiviCRMCustomType.ServiceClub.protocolName,
                    ),
                ),
            )

            if (updateRules) {
                GeneralCRMRules(civicrm).updateGeneralCrmRule(personContacts)
            }
        }
    }

    private fun createMemberGroup(): Int {
        val iogMember = credentials.getGroupByName(LdapNames.IOG_MEMBERS)!!
        val group = credentials.getGroupByName(LdapNames.CRM_MEMBERS) ?: credentials.createGroup(
            Group(
                0, LdapNames.CRM_MEMBERS, "Alle Fördermitglieder mit CRM-Zugriff",
                emptySet(), emptySet(), setOf(iogMember.id), emptySet(), emptySet(), emptySet(),
            ),
        )
        return group.id
    }

    private suspend fun createUsersGroup(): Int {
        logger.info("CiviGroupScheme::createUsersGroup Create group users.")
        val g =
            civicrm.createOrReplaceTopLevelGroupWithACLPermission(
                CiviCRMNames.USER_GROUP_NAME,
                "Alle CRM-Nutzer",
                CiviCRMNames.CRM_USER_ROLE_NAME,
            )
        civicrm.createACL("Nutzer können sich sehen", g.roleIndex, g.group.id, ACLProtocol.Operations.VIEW)
        return g.group.id
    }

    private suspend fun createEthicGroup(
        contactMainGroupId: Int,
        civicrm: ICiviCRM,
    ): Int? {
        logger.info("CiviGroupScheme::createEthicGroup Create group ${LdapNames.ETHIC_VALIDATION_GROUP}.")
        val g = civicrm.createOrReplaceSubgroupWithACLPermission(
            LdapNames.ETHIC_VALIDATION_GROUP,
            "Ethikprüfer",
            "Rolle für Ethikprüfer",
            CiviCRMNames.CRM_USER_ROLE_NAME,
        ).let { ethicGroup ->
            // civicrm.createACL("Ethikprüfer", ethicGroup.roleIndex, orgaContacts, ACLProtocol.Operations.EDIT)
            // logger.info("CreateCrmGroups::createCrmGroups create acl for Ethikprüfer with orga group id $orgaContacts ")
            civicrm.updateParentOfGroup(ethicGroup.group.id, contactMainGroupId)
        }
        return g?.id
    }

    private suspend fun createContactAdminGroup(
        credentialName: String,
        description: String,
        contactType: IogPluginConstants.CiviCRMCustomType,
        userGroupId: Int,
        contactMainGroupId: Int,
    ): Int {
        val createGroupResult =
            civicrm.createOrReplaceSubgroupWithACLPermission(
                credentialName,
                description,
                "Rolle für $description",
                CiviCRMNames.CRM_USER_ROLE_NAME,
            )
        civicrm.updateParentOfGroup(createGroupResult.group.id, userGroupId)
        civicrm.createACL(credentialName, createGroupResult.roleIndex, contactMainGroupId, ACLProtocol.Operations.EDIT)
        credentials.getGroupByName(credentialName)?.let { credentialGroup ->
            logger.info {
                "CiviGroupScheme::createContactAdminGroup found ${credentialGroup.userMembers.count()} users in group $credentialName."
            }
            credentialGroup.userMembers.forEach { userID ->
                val credentialUser = credentials.getUser(userID)
                credentialUser?.let {
                    logger.info { "CiviGroupScheme::createContactAdminGroup Import contact ${it.mail} for group $credentialName" }
                    val existingContactID = civicrm.getContactIdByEmail(it.mail)
                    if (existingContactID == null) {
                        logger.info {
                            "CiviGroupScheme::createContactAdminGroup Contact for ${it.mail} for group $credentialName does not exists. Create email, groupContact and contact with firstname ${credentialUser.firstname} and lastname ${credentialUser.surname} with type ${contactType.protocolName}."
                        }
                        runBlocking {
                            val createdContact =
                                civicrm.createContact(
                                    CiviCRMConstants.ContactTypeDefaults.Individual.protocolName,
                                    contactType.protocolName,
                                    credentialUser.firstname,
                                    credentialUser.surname,
                                )
                            civicrm.createEmail(it.mail, createdContact.id)
                            civicrm.createGroupContact(createdContact.id, createGroupResult.group.id)
                        }
                    } else {
                        logger.info {
                            "CiviGroupScheme::createContactAdminGroup Contact for ${it.mail} for group $credentialName already exists. Create only groupContact."
                        }
                        civicrm.getGroupContactsByGroupId(createGroupResult.group.id).firstOrNull { groupContact -> groupContact.contactId == existingContactID }
                            ?: civicrm.createGroupContact(existingContactID, createGroupResult.group.id)
                    }
                } ?: logger.warn { "User $userID does not exist in Credential Provider but is member of group." }
            }
        } ?: logger.warn { "$credentialName does not exist in Credential Provider." }
        return createGroupResult.group.id
    }

    fun deleteChapter(civiGroupName: String) {
        runBlocking {
            civicrm.deleteRoleWithAclDependencies(civiGroupName)
            civicrm.deleteGroupWithAclAndContactDependencies(civiGroupName)
            civicrm.deleteGroupWithAclAndContactDependencies(civiGroupName + IogPluginConstants.ORGA_SUFFIX)
            civicrm.deleteGroupWithAclAndContactDependencies(civiGroupName + IogPluginConstants.CONTACT_SUFFIX)
            civicrm.getGroupByName(CiviCRMNames.PERSON_GROUP_NAME)?.let { personContacts ->
                GeneralCRMRules(civicrm).updateGeneralCrmRule(personContacts.id)
            }
        }
    }
}
