package dev.maximilian.feather.kubernetes

public interface IKubernetes {
    public fun getNamespaces(): List<Namespace>

    public fun getNamespaceByName(name: String): Namespace?

    public fun getDeployments(
        namespace: Namespace,
        withLabels: Map<String, String> = emptyMap(),
    ): List<Deployment>

    public fun getDeploymentByName(
        namespace: Namespace,
        name: String,
    ): Deployment?

    public fun getReplicaSetsByDeployment(deployment: Deployment): List<ReplicaSet>

    public fun getPodsByReplicaSet(replicaSet: ReplicaSet): List<Pod>

    public fun executeCommand(
        pod: Pod,
        containerName: String? = null,
        commands: Array<String>,
        timeoutInSeconds: Long,
    ): CommandOutput
}
