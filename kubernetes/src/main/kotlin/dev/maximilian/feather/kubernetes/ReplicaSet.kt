package dev.maximilian.feather.kubernetes

public data class ReplicaSet(
    val name: String,
    val namespace: Namespace,
    val deployment: Deployment,
    val deploymentRevision: Int,
)
