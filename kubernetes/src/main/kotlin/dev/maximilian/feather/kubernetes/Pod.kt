package dev.maximilian.feather.kubernetes

public data class Pod(
    val name: String,
    val namespace: Namespace,
    val replicaSet: ReplicaSet,
    val deployment: Deployment,
    val ready: Boolean,
    val containers: List<String>,
)
