package dev.maximilian.feather.kubernetes

import io.fabric8.kubernetes.client.dsl.ExecListener
import mu.KotlinLogging
import java.io.ByteArrayOutputStream
import java.util.concurrent.CompletableFuture

internal class SimpleListener(
    private val data: CompletableFuture<String>,
    private val baos: ByteArrayOutputStream,
) : ExecListener {
    private val logger = KotlinLogging.logger {}

    override fun onOpen() {
        logger.debug { "Reading data... " }
    }

    override fun onFailure(
        t: Throwable,
        failureResponse: ExecListener.Response,
    ) {
        logger.error(t) { "Error while listening to command output" }
        data.completeExceptionally(t)
    }

    override fun onClose(
        code: Int,
        reason: String,
    ) {
        logger.debug { "Exit with: $code and with reason: $reason" }
        val output = baos.toString()
        logger.debug { "Output: $output" }
        data.complete(output)
    }
}
