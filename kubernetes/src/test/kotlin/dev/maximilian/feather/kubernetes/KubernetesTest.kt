package dev.maximilian.feather.kubernetes

import org.junit.jupiter.api.assertDoesNotThrow
import java.util.UUID
import kotlin.test.Test
import kotlin.test.assertContains
import kotlin.test.assertEquals
import kotlin.test.assertNull

class KubernetesTest {
    companion object {
        val k8s = Kubernetes()
        val testNamespace = Namespace(System.getenv("TEST_K8S_NAMESPACE") ?: "test")
        val testDeployment = Deployment(System.getenv("TEST_K8S_DEPLOYMENT") ?: "test", testNamespace)
    }

    @Test
    fun `List all namespaces contains test namespace`() {
        assertContains(k8s.getNamespaces(), testNamespace)
    }

    @Test
    fun `Getting non existing namespace by name returns null`() {
        assertNull(k8s.getNamespaceByName(UUID.randomUUID().toString()))
    }

    @Test
    fun `Getting existing namespace by name returns namespace`() {
        val retrieved = k8s.getNamespaceByName(testNamespace.name)
        assertEquals(testNamespace, retrieved)
    }

    @Test
    fun `List all deployments in namespace returns test deployment`() {
        assertContains(k8s.getDeployments(testNamespace), testDeployment)
    }

    @Test
    fun `Getting non existing deployment by name returns null`() {
        assertNull(k8s.getDeploymentByName(testNamespace, UUID.randomUUID().toString()))
    }

    @Test
    fun `Getting existing deployment by name returns deployment`() {
        val retrieved = k8s.getDeploymentByName(testNamespace, testDeployment.name)
        assertEquals(testDeployment, retrieved)
    }

    @Test
    fun `Getting replica sets and pods from deployment does not throw`() {
        assertDoesNotThrow {
            val replicaSet =
                checkNotNull(
                    k8s.getReplicaSetsByDeployment(testDeployment).maxByOrNull {
                        it.deploymentRevision
                    },
                ) { "Newest replica sets from test deployment not found" }
            val pods = k8s.getPodsByReplicaSet(replicaSet)
            val readyPod =
                checkNotNull(pods.firstOrNull { it.ready }) {
                    "No ready pod found"
                }

            if (System.getenv("TEST_K8S_READONLY") != "true") {
                val randomString = UUID.randomUUID().toString()
                val result =
                    k8s.executeCommand(
                        readyPod,
                        containerName = readyPod.containers.firstOrNull(),
                        arrayOf("echo", "-n", randomString),
                        15,
                    )

                assert(!result.failure)
                assertEquals(listOf(randomString), result.messages)
            }
        }
    }
}
